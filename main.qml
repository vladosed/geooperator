import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtMultimedia 5.8
import QtPositioning 5.6
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2
import Qt.labs.settings 1.0


Window {
    id: mainWindow
    visible: true
    width: 480
    height: 700
    title: qsTr("Оператор")

    property double buttonsScale: 1.1
    property double buttonsOpacity: 0.7


    /*Timer {
        interval: 2000; running: true; repeat: true
        onTriggered: {
            client.sendString("Привет!");
        }
    }*/

    RegistrationPage{
        id:registration
        anchors.fill:parent
    }

    MapPage{
        id: mapPage
        anchors.fill:parent
        buttonsScale: mainWindow.buttonsScale
        buttonsOpacity: mainWindow.buttonsOpacity
        visible: false
    }
    CameraPage{
        id: cameraPage
        anchors.fill: parent
        buttonsScale: mainWindow.buttonsScale
        buttonsOpacity: mainWindow.buttonsOpacity
        visible: false
    }
    BluetoothPage{
        id:bluetoothPage
        anchors.fill: parent
        visible: false
    }

    SyncPage{
        id: syncPage
        anchors.fill: parent
        visible: false
    }

    SyncPointPage{
        id: syncPointPage
        anchors.fill: parent
        visible: false
    }

    Image{
        id: switcher
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.leftMargin: 50
        anchors.bottomMargin: 50
        source: "qrc:/img/photo-camera.png"
        scale: buttonsScale
        opacity: buttonsOpacity
        visible: (registration.visible == false && mapPage.switcherIsVisible == true) ? true : false
        z:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if (mapPage.visible == true){
                    switcher.source = "qrc:/img/map.png"
                    mapPage.visible = false;
                    cameraPage.visible = true;
                }

                else{
                    switcher.source = "qrc:/img/photo-camera.png"
                    mapPage.visible = true;
                    cameraPage.visible = false;
                }
            }
        }

    }
    Drawer {
        id: drawer
        width: Math.min(mainWindow.width, mainWindow.height)
        height: mainWindow.height
        opacity: 0.8
        dragMargin: mainWindow.width * 0.07
        Rectangle{
            color: "black"
            anchors.fill: parent
        }

        Column{
            spacing: 20
            width: drawer.width

            Row{
                id: spins
                width: drawer.width
                spacing: 20

                Column{
                    width:drawer.width / 2
                    Label{
                        color:"white"
                        font.pointSize: 14
                        text: "Левый отступ"
                    }

                    SpinBox {
                        id: leftOffset
                        width: parent.width - spins.spacing
                        height: 100
                        from: 0
                        to:40
                        stepSize: 5
                        value: 10
                        //editable: true
                    }
                }

                Column{
                    width:drawer.width / 2
                    Label{
                        color:"white"
                        font.pointSize: 14
                        text: "Правый отступ"
                    }

                    SpinBox {
                        id: rightOffset
                        width: parent.width - spins.spacing
                        height: 100
                        from: 0
                        to:40
                        stepSize: 5
                        value: 5
                        //editable: true
                    }
                }
            }
            Button{
                id: connectButton
                width: drawer.width
                text: (client.isSocketActive == "false")? "Подключиться к серверу":
                                                          "Отключиться от сервера"
                height :100
                onClicked:{
                    if (client.isSocketActive == "false")
                        client.connectToHost(fieldIP.text);
                    else
                        client.disconnect();
                }
            }
            TextField {
                id: fieldIP
                width: drawer.width
                height: connectButton.height
                font.pointSize: 20
                enabled: (client.isSocketActive == "true")? false: true;
                inputMethodHints: Qt.ImhDigitsOnly
                validator: RegExpValidator {
                    regExp:  /^((?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){0,3}(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/
                }
            }

            Row{
                spacing: drawer.width * 0.1
                Button{
                    width: drawer.width * 0.45
                    text: "Отправить"
                    height :100
                    opacity: (client.isSocketActive == "true") ? 1 : 0
                    onClicked:{
                        client.sendData();
                        console.log (client.isSocketActive);
                        drawer.position = 0;
                    }
                }

                Button{
                    width: drawer.width * 0.45
                    text: "Получить"
                    height :100
                    opacity: (client.isSocketActive == "true") ? 1 : 0
                    onClicked:{
                        client.requestData();
                        console.log (client.isSocketActive);
                        drawer.position = 0;
                    }
                }
            }

            Row{
                spacing: drawer.width * 0.1
                Button{
                    width: drawer.width * 0.45
                    text: "Геооператор"
                    height :100
                    opacity: (client.isSocketActive == "true") ? 1 : 0
                    onClicked:{
                        client.sendData();
                        console.log (client.isSocketActive);
                        drawer.position = 0;
                    }
                }

                Button{
                    width: drawer.width * 0.45
                    text: "Управление"
                    height :100
                    opacity: (client.isSocketActive == "true") ? 1 : 0
                    onClicked:{
                        client.requestData();
                        console.log (client.isSocketActive);
                        drawer.position = 0;
                    }
                }
            }

            Button{
                width: drawer.width
                text: "Загрузить точки и границы"
                height :100
                onClicked:{
                    syncPointPage.visible = true;
                    drawer.close();
                    //btMessage.debugMessage();
                    //ftpPoints.downloadPoints();
                }
            }
            CheckBox{
                id: setPointToCenter
                indicator.width: 64
                indicator.height: 64
            }

            Row{
                spacing: 2
                Button{
                    width: drawer.width / 2
                    text: "Удалить загруженные точки"
                    height :100
                    onClicked:{
                        database.removeTemp();
                        drawer.close();
                    }
                }
                Button{
                    width: drawer.width / 2
                    text: "Удалить все точки"
                    height : 100
                    enabled: false
                    onClicked:{
                        database.removeTemp();
                        drawer.close();
                    }
                }
            }




            Button{
                width: drawer.width
                text: "Подключить к bluetooth"
                height :100
                onClicked:{
                    bluetoothPage.visible = true;
                    drawer.close();
                    //btMessage.bluetoothConnect();
                }
            }



            /*Button{
                width: drawer.width
                Layout.fillWidth: true
                text: "Привязать фото в файл"
                height :100
                onClicked: {
                    photoModel.syncLocal();
                }
            }*/

            Button{
                width: drawer.width
                Layout.fillWidth: true
                text: "Загрузить из буфера"
                height :100
                onClicked: {
                    var address = JSON.parse(clipboard.text);
                    console.log( address.lat, address.lon, address.text );
                    database.insertIntoAddressTable(address.text, address.lat, address.lon);
                    mapPage.setMapCenter(address.lat, address.lon);
                    drawer.position = 0;
                }
            }




            /*Button{
                width: drawer.width
                Layout.fillWidth: true
                text: "Сопоставить адреса в файл"
                height :100
                onClicked: {
                    //database.sync();
                    database.syncLocal();
                }
            }*/

        }



        function getLeftOffset(){
            return leftOffset.value;
        }

        function getRightOffset(){
            return rightOffset.value;
        }
    }
    Settings {
        //property alias ip: fieldIP.text
        property alias left_offset: leftOffset.value
        property alias right_offset: rightOffset.value
    }
}
