#include "client.h"

static inline QByteArray IntToArray(qint32 source);
static inline qint32 ArrayToInt(QByteArray source);

Client::Client(QObject *parent) : QObject(parent)
{
    socket = new QTcpSocket(this);
    connect(socket, &QTcpSocket::readyRead, this, &Client::readyRead);
    connect(socket, &QTcpSocket::connected, this, &Client::connected);
    connect(socket, &QTcpSocket::disconnected, this, &Client::disconnected);

    buffer = new QByteArray();
}

void Client::sendString(const QString & str)
{
    double lat = 35.7f;
    double lon = 56.2f;
    TcpPack pack(lat, lon, "XUY");

    qDebug() << "Sent";

    this->writeData(pack.getPack());
}

bool Client::connectToHost(QString host)
{
    socket->connectToHost(host, 6000);
    return socket->waitForConnected();
}

bool Client::writeData(QByteArray data)
{
    if(socket->state() == QAbstractSocket::ConnectedState)
    {
        socket->write(IntToArray(data.size())); //write size of data
        socket->write(data); //write the data itself
        return socket->waitForBytesWritten();
    }
    else
        return false;
}

void Client::requestData()
{
    TcpPack pack;
    pack.setGetType();

    qDebug() << "Запрос на получение точек";

    this->writeData(pack.getPack());
}


void Client::sendData()
{
    qDebug() << "Запрос на отправку точек";

    TcpPack startPack;
    startPack.setSendType();
    this->writeData(startPack.getPack());


    QSqlQuery query("SELECT *  FROM addresses");

    while (query.next()) {
        QString id = query.value(0).toString();
        QString address = query.value(1).toString();
        QString lat = query.value(2).toString();
        QString lon = query.value(3).toString();
        qDebug() << id << " " << address << " " << lat << " " << lon;

        TcpPack coordPack(lat.toDouble(), lon.toDouble(), address);
        this->writeData(coordPack.getPack());
    }

    TcpPack endPack;
    endPack.setEndType();
    this->writeData(endPack.getPack());
}

bool Client::readyRead()
{
    qDebug() << "recieve";
    while (socket->bytesAvailable() > 0)
    {
        buffer->append(socket->readAll());

        while ((size == 0 && buffer->size() >= 4) || (size > 0 && buffer->size() >= size)) //While can process data, process it
        {
            if (size == 0 && buffer->size() >= 4) //if size of data has received completely, then store it on our global variable
            {

                size = ArrayToInt(buffer->mid(0, 4));
                buffer->remove(0, 4);
            }
            if (size > 0 && buffer->size() >= size) // If data has received completely, then emit our SIGNAL with the data
            {

                QByteArray data = buffer->mid(0, size);
                buffer->remove(0, size);
                size = 0;

                qDebug() << data.size();
                TcpPack pack;
                if(pack.parsePack(data) == POINT_TYPE){
                    emit getAddress(pack);
                    qDebug() << pack.lat() << pack.lon() << pack.str();
                }
            }
        }
    }
    return true;
}

void Client::disconnect()
{
    socket->disconnectFromHost();
}

QString Client::getSocketActive()
{
    if (isSocketActive)
        return QString("true");
    else
        return QString("false");
}

void Client::connected()
{
    isSocketActive = (socket->state() == QTcpSocket::ConnectedState);
    qDebug() << "Дарова!";
    emit socketActiveChange();
}

void Client::disconnected()
{
    isSocketActive = (socket->state() == QTcpSocket::ConnectedState);
    emit socketActiveChange();
}

QByteArray IntToArray(qint32 source) //Use qint32 to ensure that the number have 4 bytes
{
    //Avoid use of cast, this is the Qt way to serialize objects
    QByteArray temp;
    QDataStream data(&temp, QIODevice::ReadWrite);
    data << source;
    return temp;
}

qint32 ArrayToInt(QByteArray source)
{
    qint32 temp;
    QDataStream data(&source, QIODevice::ReadWrite);
    data >> temp;
    return temp;
}

