#ifndef TCPPACK_H
#define TCPPACK_H

#include <QByteArray>
#include <QString>

#define GET_DATA_TYPE 1
#define POINT_TYPE 2
#define END_TYPE 3
#define SEND_DATA_TYPE 4

class TcpPack
{
public:
    TcpPack();

    TcpPack(double lat, double lon, const QString & str){
        this->m_type = POINT_TYPE;
        this->m_lat = lat;
        this->m_lon = lon;
        this->m_str = str;
    }

    void setLat(double lat){
        this->m_lat = lat;
    }

    void setLon(double lon){
        this->m_lon = lon;
    }

    void setType(int type){
        this->m_type = type;
    }

    void setStr(const QString & str){
        this->m_str = str;
    }

    double lat(){
        return this->m_lat;
    }

    double lon(){
        return this->m_lon;
    }

    int type(){
        return this->m_type;
    }

    QString str(){
        return this->m_str;
    }

    QByteArray getPack();
    int parsePack(QByteArray array);
    void setGetType();
    void setSendType();
    void setEndType();

private:
    double m_lat, m_lon;
    int m_type;
    QString m_str;
};

#endif // TCPPACK_H
