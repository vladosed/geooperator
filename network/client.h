#ifndef CLIENT_H
#define CLIENT_H


#include <QtCore>
#include <QtNetwork>
#include <QSqlQuery>
#include <QSqlError>

#include "tcppack.h"

class Client : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString isSocketActive READ getSocketActive NOTIFY socketActiveChange)
public:
    explicit Client(QObject *parent = 0);
    Q_INVOKABLE void sendString(const QString &str);

public slots:
    bool connectToHost(QString host);
    bool writeData(QByteArray data);

    void requestData();
    void sendData();

    bool readyRead();
    void disconnect();
    QString getSocketActive();
    void connected();
    void disconnected();
signals:
    void getAddress(TcpPack pack);
    void socketActiveChange();

private:
    QTcpSocket * socket;
    QByteArray * buffer; //We need a buffer to store data until block has completely received
    qint32 size = 0;
    bool isSocketActive = false;
};

#endif // CLIENT_H
