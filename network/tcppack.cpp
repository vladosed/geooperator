#include "tcppack.h"

TcpPack::TcpPack()
{

}

int TcpPack::parsePack(QByteArray array)
{
    if (array.size() < sizeof(m_type))
        return -1;
    m_type = *reinterpret_cast<const int*>(array.data());
    array.remove(0, sizeof(m_type));

    if (m_type == POINT_TYPE){

        if (array.size() < sizeof(m_lat))
            return -2; //error lat
        m_lat = *reinterpret_cast<const double*>(array.data());
        array.remove(0, sizeof(m_lat));

        if (array.size() < sizeof(m_lon))
            return -2; //error lon
        m_lon = *reinterpret_cast<const double*>(array.data());
        array.remove(0, sizeof(m_lon));

        m_str = QString(array);
    }

    return m_type;

}

void TcpPack::setGetType()
{
    m_type = GET_DATA_TYPE;
}

void TcpPack::setSendType()
{
    m_type = SEND_DATA_TYPE;
}

void TcpPack::setEndType()
{
    m_type = END_TYPE;
}

QByteArray TcpPack::getPack(){
    QByteArray array;
    array.append(reinterpret_cast<const char*>(&m_type), sizeof(m_type));
    array.append(reinterpret_cast<const char*>(&m_lat), sizeof(m_lat));
    array.append(reinterpret_cast<const char*>(&m_lon), sizeof(m_lon));
    array.append(m_str);

    return array;
}
