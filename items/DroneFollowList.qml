import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {
    Rectangle{
        id: body
        anchors.fill: parent
        height: parent.height
        color: "white"
        opacity: 0.7

        ListView{
            anchors.fill:parent
            model:droneModel

            property int itemSize: 70
            highlight: Rectangle { color: "gray"; opacity: 0.2 }
            delegate: RowLayout{
                width:parent.width
                id: delegate
                property int cols: 4
                Layout.fillWidth: parent
                Text{
                    text: name
                    font.pixelSize: 50
                    Layout.minimumWidth: body.width * 0.5
                    Layout.maximumWidth: body.width * 0.5
                    horizontalAlignment: Text.AlignLeft
                }
                MouseArea{
                    anchors.fill:parent
                    onPressed: {
                        delegate.ListView.view.currentIndex = index;
                    }
                    onClicked: {

                    }
                    onPressAndHold: {
                        setMapCenter(lat, lon)
                    }
                }
            }
            z:2
        }
        z:2

    }

}
