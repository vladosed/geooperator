import QtQuick 2.0
import QtQuick.Controls 2.2

Item {
    id: photoAdder
    TextField{
        id: photoComment
        anchors.fill:photoAdder
        visible: photoPreview.visible
    }

    Image{
        id: confirmButton
        anchors.right: photoComment.right
        anchors.verticalCenter: photoComment.verticalCenter
        width: photoComment.height * 0.8
        height: photoComment.height * 0.8
        visible: photoPreview.visible
        source: "qrc:/img/ok.png"

        z:3
        MouseArea{
            anchors.fill: parent
            onClicked:{
                cameraPage.savePhoto(photoComment.text);

                photoComment.clear();
                photoComment.focus = false;
                photoPreview.visible = false;
                switcher.visible = true;

            }
        }
    }
    Image{
        anchors.right: confirmButton.left
        anchors.rightMargin: 60
        anchors.verticalCenter: photoComment.verticalCenter
        width: photoComment.height * 0.6
        height: photoComment.height * 0.6
        visible: photoPreview.visible
        source: "qrc:/img/delete.png"
        z:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                photoComment.text = "";
                photoComment.focus = false;
                photoPreview.visible = false;
                switcher.visible = true;
            }
        }
    }

    function setFocus(){
        photoComment.focus = true;
    }
}
