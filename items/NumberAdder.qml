import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import ".."

Item {
    z:3
    property string lastNumber: "-1"
    property double lat: -1
    property double lon: -1
    property bool customSelection: false

    property bool isPhoto: false
    property string photo_src: ""

    property bool isChangeActive: false
    property string activeText : ""



    TextField{
        id: homeNumber

        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width
        height: parent.height * 0.20

        text: ""
        //inputMethodHints: Qt.ImhDigitsOnly || Qt.ImhMultiLine
        font.pixelSize: (homeNumber.height - 10) * 0.7
        z:3

        function confirm(){
            cancelFocus();
            switcher.visible = true;

            var n = parseInt(homeNumber.text);
            var last = parseInt(lastNumber);


            if (last != -1){
                if (isPhoto) // если item не используется для описания фото
                    database.insertIntoPhotoTable(parent.lat, parent.lon, parent.photo_src, homeNumber.text);
                else if(isChangeActive){
                    if (latitude.text != "" || longitude.text != ""){
                        database.changeActiveAddress(homeNumber.text, latitude.text, longitude.text);
                        latitude.text = "";
                        longitude.text = "";
                    }
                    else
                        database.changeActiveAddress(homeNumber.text);
                }
                else
                    database.insertIntoAddressTable(homeNumber.text, parent.lat, parent.lon);

                if(!isChangeActive){ // если item не используется для смены значения активной точки

                    if(Math.abs(n - last) <= 2){
                        console.log((n + (n - last) ).toString());
                        console.log(homeNumber.text, n, last, n + (n - last));
                        homeNumber.text = ( n + (n - last) ).toString();
                        mapPage.activeTextOfNewPoint(homeNumber.text);
                    }
                    lastNumber = n.toString();
                    if ((n - last)>2){
                        lastNumber = -1;
                    }
                }
            }
            else{
                if (isPhoto) // если item не используется для описания фото
                    database.insertIntoPhotoTable(parent.lat, parent.lon, parent.photo_src, homeNumber.text);
                else if(isChangeActive)
                    if (latitude.text != "" || longitude.text != ""){
                        database.changeActiveAddress(homeNumber.text, latitude.text, longitude.text);
                        lat = latitude.text;
                        lon = longitude.text;
                    }
                    else
                        database.changeActiveAddress(homeNumber.text);
                else
                    database.insertIntoAddressTable(homeNumber.text, parent.lat, parent.lon);

                if(!isChangeActive)// если item не используется для смены значения активной точки
                    lastNumber = homeNumber.text;
                    mapPage.activeTextOfNewPoint(homeNumber.text);
            }
        }

        Keys.onPressed: {
            // 16777220 - Enter code
            if(event.key === 16777220){
                confirm();
            }
        }
        onFocusChanged: {
            console.log(focus);
            if (focus == false){
                //parent.visible = false;
            }
        }
    }

    TextField{
        id: longitude
        anchors.bottom: homeNumber.top
        anchors.left: parent.left
        width: parent.width
        height: parent.height * 0.20

        inputMethodHints: Qt.ImhDigitsOnly

        visible: isChangeActive

        //text: parent.lon.toString()
        //inputMethodHints: Qt.ImhDigitsOnly || Qt.ImhMultiLine
        font.pixelSize: (homeNumber.height - 10) * 0.7
        z:3
    }
    TextField{
        id: latitude
        anchors.bottom: longitude.top
        anchors.left: parent.left
        width: parent.width
        height: parent.height * 0.20
        inputMethodHints: Qt.ImhDigitsOnly

        visible: isChangeActive

        //text: parent.lat.toString()
        font.pixelSize: (homeNumber.height - 10) * 0.7
        z:3
    }

    Image{
        id: confirmButton
        anchors.right: homeNumber.right
        anchors.verticalCenter: homeNumber.verticalCenter
        width: homeNumber.height * 0.8
        height: homeNumber.height * 0.8
        source: "qrc:/img/ok.png"
        z:4
        MouseArea{
            anchors.fill: parent
            onClicked: homeNumber.confirm();
        }
    }

    Image{
        anchors.right: confirmButton.left
        anchors.rightMargin: 60
        anchors.verticalCenter: homeNumber.verticalCenter
        width: homeNumber.height * 0.8
        height: homeNumber.height * 0.8
        source: "qrc:/img/delete.png"
        visible: isChangeActive
        z:4
        MouseArea{
            anchors.fill: parent
            onClicked: {
                database.removeActiveRecord();
                cancelFocus();
            }
        }
    }


    Rectangle{
        id: fastPanel
        color: "#ebeff2"

        anchors.top: homeNumber.bottom
        anchors.left: parent.left
        width: parent.width
        height: parent.height - homeNumber.height
        z:3

        GridLayout {
            width: parent.width
            height: parent.height
            Layout.margins: 5

            columns: 3
            Button { text: "А"; Layout.alignment: Qt.AlignCenter; Layout.fillWidth: true; Layout.fillHeight: true; onClicked: {setText(text)}}
            Button { text: "Б"; Layout.alignment: Qt.AlignCenter; Layout.fillWidth: true; Layout.fillHeight: true; onClicked: {setText(text)} }
            Button { text: "В"; Layout.alignment: Qt.AlignCenter; Layout.fillWidth: true; Layout.fillHeight: true; onClicked: {setText(text)} }

            Button { text: "1"; Layout.alignment: Qt.AlignCenter; Layout.fillWidth: true; Layout.fillHeight: true; onClicked: {setText(text)}}
            Button { text: "2"; Layout.alignment: Qt.AlignCenter; Layout.fillWidth: true; Layout.fillHeight: true; onClicked: {setText(text)} }
            Button { text: "3"; Layout.alignment: Qt.AlignCenter; Layout.fillWidth: true; Layout.fillHeight: true; onClicked: {setText(text)} }

            Button { text: "4"; Layout.alignment: Qt.AlignCenter; Layout.fillWidth: true; Layout.fillHeight: true; onClicked: {setText(text)} }
            Button { text: "5"; Layout.alignment: Qt.AlignCenter; Layout.fillWidth: true; Layout.fillHeight: true; onClicked: {setText(text)} }
            Button { text: "6"; Layout.alignment: Qt.AlignCenter; Layout.fillWidth: true; Layout.fillHeight: true; onClicked: {setText(text)} }

            Button { text: "7"; Layout.alignment: Qt.AlignCenter; Layout.fillWidth: true; Layout.fillHeight: true; onClicked: {setText(text)} }
            Button { text: "8"; Layout.alignment: Qt.AlignCenter; Layout.fillWidth: true; Layout.fillHeight: true; onClicked: {setText(text)} }
            Button { text: "9"; Layout.alignment: Qt.AlignCenter; Layout.fillWidth: true; Layout.fillHeight: true; onClicked: {setText(text)} }

            Button { text: "Буф";  Layout.alignment: Qt.AlignCenter; Layout.fillWidth: true; Layout.fillHeight: true; onClicked: {clipboard.set('{"lat":"'+lat+'","lon":"'+lon+'","text":"'+homeNumber.text+'"}');} }
            Button { text: "0"; Layout.alignment: Qt.AlignCenter; Layout.fillWidth: true; Layout.fillHeight: true; onClicked: {setText(text)} }
            Button { text: "<";  Layout.alignment: Qt.AlignCenter; Layout.fillWidth: true; Layout.fillHeight: true; onClicked: {homeNumber.text = homeNumber.text.slice(0, -1)
                }
            }
        }
    }

    function setFocus(){
        homeNumber.forceActiveFocus();
        homeNumber.focus = true;
        homeNumber.selectAll();
        //switcher.visible = true;
    }
    function selectAll(){
        customSelection = true;
        if(isChangeActive){
            homeNumber.text = activeText;
        }
        homeNumber.selectAll();

        if(isChangeActive){
            longitude.text = lon.toFixed(6);
            latitude.text = lat.toFixed(6);
        }
    }
    function setText(text){
        if (customSelection){
            customSelection = false;
            homeNumber.text = text
        }
        else{
            homeNumber.text += text
        }
    }
    function cancelFocus(){
        mapPage.visibilityControls = true;
        mapPage.hideTempMarkers();
        visible = false;
        homeNumber.focus = false;
        latitude.focus = false;
        longitude.focus = false;
        switcher.visible = true;
        if (isPhoto){
            photoPreview.visible = false
        }
    }
}
