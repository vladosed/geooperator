import QtQuick 2.0
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.Controls 2.2

Item {
    property bool table_visible: false;
    property double main_opacity: 0.8;
    id: mainItem
    height: table_visible == false ? 40 : parent.height

    Rectangle{
        id: header
        anchors.top: parent.top
        anchors.left: parent.left
        width:parent.width
        height: 100
        opacity: main_opacity

        GridLayout{
            anchors.fill: header
            columns: 3
            property int textSize: header.height * 0.40
            Text{
                id: drone_alt
                text: "В " + Math.round(droneModel.alt)
                font.pixelSize: parent.textSize

                Layout.maximumWidth: header.width/3
                Layout.maximumHeight:  header.height/3
            }
            Text{
                id: drone_photo_number
                text:  droneModel.photoNumber
                font.pixelSize: parent.textSize

                Layout.maximumWidth: header.width/3
                Layout.maximumHeight:  header.height/3
            }
            Text{
                id: drone_weather
                text:  droneModel.weather.toFixed(1)
                font.pixelSize: parent.textSize

                Layout.maximumWidth: header.width/3
                Layout.maximumHeight:  header.height/3
            }
            Text{
                text: btMessage.terminal_charge
                color: btMessage.comColor
                font.pixelSize: parent.textSize

                Layout.column: 2
                Layout.row: 1
                Layout.maximumWidth: header.width/3
                Layout.maximumHeight:  header.height/3
            }

        }

        MouseArea{
            anchors.fill:parent
            onClicked: {
                if (table_visible == false){
                    table_visible = true;
                }
                else{
                    table_visible = false;
                }


            }
            z:3
        }
        z:3
    }

    Rectangle{
        id: repeater
        anchors.top:  header.bottom
        anchors.left: parent.left
        width:parent.width
        height: 70
        visible: (table_visible)  && (btMessage.ret_actual)
        opacity: main_opacity
        property int textSize: repeater.height * 0.9
        RowLayout{
            anchors.fill: parent
            anchors.horizontalCenter: parent.horizontalCenter
            Text{
                id: repeater_laber
                text: "RET"
                font.pixelSize: repeater.textSize
            }

            Text{
                id: repeater_speed
                text: btMessage.ret_charge
                font.pixelSize: parent.textSize

            }
        }
        z:3
    }

    Rectangle{
        id: body
        anchors.top: (btMessage.ret_actual) ? repeater.bottom: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height
        visible: table_visible
        color: "white"
        opacity: main_opacity

        ListView{
            anchors.fill:parent
            model:droneModel

            property int itemSize: 70
            highlight: Rectangle { color: "gray"; opacity: 0.2 }
            delegate: RowLayout{
                width:parent.width
                id: delegate
                property int cols: 4
                Layout.fillWidth: parent
                Text{
                    color: checkDroneColor(elapsed, lat, lon)
                    text: (repeater == 2) ? "*" + name : "" + name
                    font.pixelSize: 50
                    Layout.minimumWidth: body.width * 0.5
                    Layout.maximumWidth: body.width * 0.5
                    horizontalAlignment: Text.AlignLeft
                }
                Text{
                    color: checkCharge(battery)
                    font.pixelSize: 50
                    text: battery
                    horizontalAlignment: Text.AlignRight
                    Layout.minimumWidth: body.width * 0.2
                    Layout.maximumWidth: body.width * 0.2
                }
                Text{
                    color: checkDroneColor(elapsed, lat, lon)
                    font.pixelSize: 50
                    text: sat
                    horizontalAlignment: Text.AlignRight
                    Layout.minimumWidth:  body.width * 0.2
                    Layout.maximumWidth:  body.width * 0.2
                    Layout.alignment: Qt.AlignRight
                }
                MouseArea{
                    anchors.fill:parent
                    onPressed: {
                        delegate.ListView.view.currentIndex = index;
                    }
                    onClicked: {   
                        btMessage.setDisplayName(name, alt, photo, weather);
                    }
                    onPressAndHold: {
                        setMapCenter(lat, lon)

                        if (setPointToCenter.checked){
                            adderCenter.visible = true;
                            //adderCenter.setFocus();
                            adderCenter.lat = lat;
                            adderCenter.lon = lon;
                            adderCenter.selectAll();
                            adderCenter.setText(name)
                        }


                    }
                }
            }
            z:2
        }
        z:2

    }

    function checkDroneColor (elapse, lat, lon){
        if (elapse > 15000 )
            return "red";
        else if (lat == 0.0 || lon == 0.0)
            return "gray";
        else
            return "black";
    }
    function checkCharge (charge){
        charge = parseFloat(charge)
        if (charge < 3.4)
            return "red";
        else if (charge <= 3.6 && charge >= 3.4)
            return "orange";
        else
            return "green";
    }

    function checSat (sat){
        charge = parseFloat(charge)
        if (sat < 6)
            return "red";
        else if (sat <= 9 && sat >= 6)
            return "orange";
        else
            return "green";
    }

    Settings {
        property alias table_visible: mainItem.table_visible
    }
}
