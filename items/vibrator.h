#ifndef VIBRATOR_H
#define VIBRATOR_H
#include <QObject>

#if defined(Q_OS_ANDROID)
#endif

class Vibrator : public QObject
{
    Q_OBJECT

public:
    explicit Vibrator(QObject *parent = 0);

signals:

public slots:
    void vibrate(int milliseconds);

private:
};

#endif // VIBRATOR_H
