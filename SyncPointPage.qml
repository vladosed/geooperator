import QtQuick 2.0
import QtQuick.Controls 2.4

Item {
    id:page
    property var groups
    property var points: []
    property var lines: []
    function getFriends() {
        var request = new XMLHttpRequest()
        request.open('GET', 'http://gvovav.fvds.ru/public/api/v1/group')
        request.onreadystatechange = function() {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status && request.status === 200) {

                    var result = JSON.parse(request.responseText)
                    page.groups = result
                    console.log(result[0].elements[0].points);
                } else {
                    console.log("HTTP:", request.status, request.statusText)
                }
            }
        }
        request.setRequestHeader('Accept', 'application/json')
        request.send()
    }

    Rectangle{
        id: body
        anchors.top: parent.top
        anchors.bottom: buttonsSync.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height
        color: 'skyblue'
        opacity: 0.8

        ListView {
            id: view

            anchors.margins: 10
            anchors.fill: parent
            model: groups
            spacing: 10

            delegate: Rectangle {
                width: view.width
                height: 100
                anchors.horizontalCenter: parent.horizontalCenter
                color: 'white'
                border {
                    color: 'lightgray'
                    width: 2
                }
                radius: 10

                Row {
                    anchors.margins: 10
                    anchors.fill: parent
                    spacing: 10

                    Text {
                        width: parent.width - parent.spacing
                        anchors.verticalCenter: parent.verticalCenter
                        elide: Text.ElideRight
                        renderType: Text.NativeRendering
                        text: "%1".arg(modelData['name'])
                    }
                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        syncPointPage.visible = false;
                        setPointsAndLines(modelData['id']);
                    }
                }
            }
        }

    }
    Row{
        id:buttonsSync
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        height: 100
        spacing: 4
        Button{
            width: parent.width / 2
            height: 100;
            onClicked: syncPointPage.visible=false
            text: "Закрыть"
        }
        Button{
            height: 100;
            width: parent.width / 2
            onClicked: getFriends()
            text: "Обновить"
        }
    }
    function setPointsAndLines(id){
        var group

        points = []
        lines = []

        groups.forEach(function(element) {
            if (element.id == id){
                group = element;
            }
        });

        group.elements.forEach(function(element) {
            if (element.type == "line"){
                var pth = []
                element.points.forEach(function(point) {
                    pth.push({latitude: point.lat , longitude: point.lon})
                });
                lines.push({
                               path:pth,
                               width: element.width,
                               color: element.color
                           })
            }
            if (element.type == "point"){
                var point = element.points[0]
                point['name'] = element.name
                //console.log(point.name, point.lat, point.lon)
                // points.push(element.points[0])'
                if (point.name == "")
                    point.name = " "
                database.insertIntoAddressTableTemp(point.name, point.lat, point.lon)
            }
        });


        mapPage.setAddressLines(lines);
        mapPage.setAddressPoints(points);
    }

    Component.onCompleted: {
        getFriends()
    }
}


