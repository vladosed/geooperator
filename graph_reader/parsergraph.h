#ifndef PARSERGRAPH_H
#define PARSERGRAPH_H
#include <QObject>
#include <QString>
#include <QDebug>

enum { WAIT_START, READING, MESSAGE_ERROR, MESSAGE_CORRECT};

#define RETR_ON 2

class ParserGraph : public QObject
{
    Q_OBJECT
public:
    virtual ~ParserGraph() {}
    explicit ParserGraph();
    bool parseChar(char chr);
    bool parseMessage();
    QVariant lat() { return this->m_lat; }
    QVariant lon() { return this->m_lon; }
    QVariant name() { return this->m_name; }
    QVariant alt() { return this->m_alt; }
    QVariant course() { return this->m_course; }
    QVariant weather() { return this->m_weather; }
    void startReading();
    QVariant getValue(QStringList &list, QString value_name);



private:
    QString package;
    int stage = WAIT_START;
    int Num = 0;
    bool Actual = 0;
    unsigned long age;

    QVariant m_name;
    QVariant m_lat = 0.0; // Вновь получаемые координаты
    QVariant m_lon = 0.0;
    QVariant m_alt = 0.0; //Высота
    QVariant m_speed = 0.0; //скорость
    QVariant m_course = 0.0; //Курс - угол направления
    QVariant m_weather = -1.0;
    QVariant m_battery = 0;
    QVariant m_repeater = 0;
    QVariant m_term_bat = 0;
    QVariant m_photoNumber = 0;
    QVariant m_satCount = 0;

signals:
    void droneMessage(double lat,
                      double lon,
                      double course,
                      double weather,
                      double alt,
                      int battery,
                      int repeater,
                      const QString name,
                      int term_bat,
                      int photoNumber,
                      int satCount);
    void repeaterMessage(int charge, int status);

};

#endif // PARSERGRAPH_H
