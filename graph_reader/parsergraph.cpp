#include "parsergraph.h"

ParserGraph::ParserGraph()
{
    this->package = "";

}

bool ParserGraph::parseChar(char chr)
{
    if (this->stage == WAIT_START){

        if (chr == '$'){
            this->stage = READING;
            return false;
        }

        return false;
    }

    if(stage == READING){


        if(chr == '*'){
            this->stage = WAIT_START;
            return this->parseMessage();
        }

        this->package.append(chr);
        return false;
    }

    return false;

}

bool ParserGraph::parseMessage()
{
    this->package = this->package.replace(QString("\t"), QString(" "));
    this->package = this->package.replace(QString("\n"), QString(" "));



    auto msg_list = this->package.split(" ", QString::SkipEmptyParts);


    if(msg_list.size() == 3){ // repeater
        int charge = this->getValue(msg_list, "BR").toInt();
        int status = this->getValue(msg_list, "status").toInt();
        emit repeaterMessage(charge, status);

        this->startReading();
        return false;
    }

    if(msg_list.size() <= 3)
        return false;


    this->m_name = this->getValue(msg_list, "Name");
    this->m_lat = this->getValue(msg_list, "lat");
    this->m_lon = this->getValue(msg_list, "lon");
    this->m_alt = this->getValue(msg_list, "H");
    this->m_course = this->getValue(msg_list, "course");
    this->m_battery = this->getValue(msg_list, "BM");
    this->m_repeater = this->getValue(msg_list, "RETR");
    this->m_term_bat = this->getValue(msg_list, "BT");
    this->m_photoNumber= this->getValue(msg_list, "Photo");
    this->m_satCount = this->getValue(msg_list, "Sat");

    QVariant defect_value(-1);
    QVariant weather_from_list = this->getValue(msg_list, "Pog");

    if (weather_from_list != defect_value){
        qDebug() << "weather: " << weather_from_list;
        this->m_weather = weather_from_list.toFloat();
    }
    else{
        this->m_weather = -1.0f;
    }

    emit droneMessage(this->m_lat.toDouble(),
                    this->m_lon.toDouble(),
                    this->m_course.toDouble(),
                    this->m_weather.toDouble(),
                    this->m_alt.toDouble(),
                    this->m_battery.toInt(),
                    this->m_repeater.toInt(),
                    this->m_name.toString(),
                    this->m_term_bat.toInt(),
                    m_photoNumber.toInt(),
                    this->m_satCount.toInt());

    this->startReading();

    return true;
}

void ParserGraph::startReading()
{
    this->stage = WAIT_START;
    this->package = "";
}

QVariant ParserGraph::getValue(QStringList &list, QString value_name)
{
    for ( const auto& i : list  )
    {
        auto temp_value = i.split(":", QString::SkipEmptyParts);

        if (value_name == QString("RETR")){
            bool isRetr = (temp_value.at(0) == "ORE") || (temp_value.at(0) == "NRE");
            if( ( temp_value.size() == 1 ) && isRetr)
                return RETR_ON;
        }

        if( ( temp_value.at(0) == value_name ) && ( temp_value.size() >= 2 )){
            return QVariant::fromValue(temp_value.at(1));
            qDebug() << temp_value.at(0) << temp_value.at(1) << endl;
        }
    }
    return QVariant::fromValue(-1);
}
