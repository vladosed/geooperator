#include "Communicator.h"

Communicator::Communicator(QObject *parent) : QObject(parent)
{

    /*QFile file("E:/pack/telem_from_copter.bin");
    if (!file.open(QIODevice::ReadOnly)) return;
    QByteArray blob = file.readAll();

    qDebug() << blob;*/
   /*QBluetoothLocalDevice localDevice;
    localDevice.powerOn();

    socket = new QBluetoothSocket(QBluetoothServiceInfo::RfcommProtocol);
    connect(socket, SIGNAL(readyRead()), this, SLOT(readBluetooth()));


    QString string("20:16:12:01:03:37");
    static const QBluetoothUuid  serviceUUID(QBluetoothUuid::SerialPort);
    socket->connectToService(QBluetoothAddress(string), serviceUUID, QIODevice::ReadOnly);*/
    connect(&parser, &ParserGraph::droneMessage, this, &Communicator::parsedDroneMessage);
    connect(&parser, &ParserGraph::repeaterMessage, this, &Communicator::parsedRepeaterMessage);
}

QVariant Communicator::getCharge()
{
    double val = (this->m_retCharge.toDouble() * 5.0 ) / 255.0;
    return QString::number(val, 'f', 1);
}

QVariant Communicator::getActual()
{
    if(retr_elapsed == 100000)
        return false;

    return (time.elapsed() - retr_elapsed < 60000) ? true : false;
}

QVariant Communicator::getTerminalCharge()
{
    double val = (this->m_term_charge.toDouble() * 5.0 ) / 255.0;
    return QString::number(val, 'f', 1);
}

QVariant Communicator::getComColor()
{
    if (this->m_btConnect == false){
        return "gray";
    }
    else if (getTerminalCharge().toDouble() < 3.2){
        qDebug() << this->getCharge().toDouble();
        return "red";
    }
    else
        return "green";

}



void Communicator::readBluetooth()
{

    if(!socket)
        return;


    QByteArray datagram;

    while (socket->bytesAvailable() > 0) {

        datagram.resize(socket->bytesAvailable());
        socket->read(datagram.data(), datagram.size());

        QString s_data = QString::fromUtf8(datagram.data());

        for (int pos = 0; pos < s_data.length(); ++pos)
        {
            if (parser.parseChar(s_data.at(pos).toLatin1())){
                //reveived.append(s_data);

                QString str;
                str.append(" lat: ");
                str.append(parser.lat().toString());
                str.append(" lon: ");
                str.append(parser.lon().toString());

                /*this->m_lat = parser.lat();
                this->m_lon = parser.lon();
                this->m_course = parser.course();
                this->m_weather = parser.weather();
                this->m_alt = parser.alt();

                emit newMessage(this->m_lat.toDouble(),
                                this->m_lon.toDouble(),
                                this->m_course.toDouble(),
                                this->m_weather.toDouble(),
                                this->m_alt.toDouble(),
                                parser.name().toString());*/

                //qDebug() << ">>>>>>>>>>>>>" <<  parser.lat() << " " << parser.lon() << " " << parser.course() << " " << parser.weather() << " " << parser.name();

                //emit latChanged(); emit lonChanged(); emit courseChanged();

                reveived.append(str);
                /*this->m_pitch = reveived;
                pitchChanged();*/
            }
        }
    }

}

void Communicator::bluetoothConnect(QString address)
{
    QBluetoothLocalDevice localDevice;
    localDevice.powerOn();

    socket = new QBluetoothSocket(QBluetoothServiceInfo::RfcommProtocol);
    connect(socket, SIGNAL(readyRead()), this, SLOT(readBluetooth()));
    connect(socket, &QBluetoothSocket::connected, this, &Communicator::btConnect);
    connect(socket, &QBluetoothSocket::disconnected, this, &Communicator::btDisconnect);


    QString string(address);
    static const QBluetoothUuid  serviceUUID(QBluetoothUuid::SerialPort);
    socket->connectToService(QBluetoothAddress(string), serviceUUID, QIODevice::ReadOnly);
}

void Communicator::debugMessage()
{
    QByteArray message;
    socket->write(message);
}

void Communicator::updateRetr()
{
    emit actualChanged();
}

void Communicator::setDisplayName(QString droneName, QVariant alt, QVariant numberPhoto, QVariant weather)
{
    this->droneName = droneName;
    emit headerChanged(alt, numberPhoto, weather);
}

void Communicator::setTrackedDroneName(QString droneName, QVariant lat, QVariant lon)
{
    this->droneName = droneName;
    emit trackedDroneChanged(lat, lon);
}

void Communicator::elapsed_retr()
{
    this->retr_elapsed = time.elapsed();
}

void Communicator::btConnect()
{
    this->m_btConnect = true;
    emit terminalColorChanged();
}

void Communicator::btDisconnect()
{
    this->m_btConnect = false;
    emit terminalColorChanged();
}

void Communicator::parsedDroneMessage(double lat, double lon, double course, double weather, double alt, int battery, int repeater, const QString name, int term_bat, int photoNumber, int satCount)
{
    if(name == droneName.toString())
        emit headerChanged(alt, photoNumber, weather);

    if(name == trackedDroneName.toString())
        emit trackedDroneChanged(lat, lon);

    this->m_term_charge = term_bat;
    emit terminalColorChanged();

    emit terminalChargeChanged();
    emit newDroneMessage(lat,
                    lon,
                    course,
                    weather,
                    alt,
                    battery,
                    repeater,
                    photoNumber,
                    satCount,
                    name);

}

void Communicator::parsedRepeaterMessage(int charge, int status)
{
    this->m_retCharge = QVariant::fromValue(charge);
    this->m_retStatus = QVariant::fromValue(status);
    qDebug() << "signal is working " << this->m_retCharge;
    time.start();
    elapsed_retr();
    emit actualChanged();
    emit chargeChanged();
}


