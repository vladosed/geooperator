#ifndef MAVCOMMUNICATOR_H
#define MAVCOMMUNICATOR_H

#include <QObject>
#include <QDebug>
#include <QtMath>
#include <QTimer>
#include <QTime>
#include <QFile>

#include <QtBluetooth/QBluetoothSocket>
#include <QtBluetooth/QBluetoothDeviceDiscoveryAgent>
#include <QtBluetooth/QBluetoothLocalDevice>

#include "parsergraph.h"



class Communicator : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariant ret_charge READ getCharge NOTIFY chargeChanged)
    Q_PROPERTY(QVariant ret_actual READ getActual NOTIFY actualChanged)
    Q_PROPERTY(QVariant terminal_charge READ getTerminalCharge NOTIFY terminalChargeChanged)
    Q_PROPERTY(QVariant comColor READ getComColor NOTIFY terminalColorChanged)

public:
    virtual ~Communicator() {}
    explicit Communicator(QObject *parent = nullptr);

    QVariant getCharge();
    QVariant getActual();
    QVariant getTerminalCharge();
    QVariant getComColor();

signals:
    void chargeChanged();
    void actualChanged();
    void courseChanged();
    void terminalChargeChanged();
    void terminalColorChanged();
    void headerChanged(QVariant alt, QVariant numberPhoto, QVariant weather);
    void trackedDroneChanged(QVariant lat, QVariant lon);
    void newDroneMessage(double lat,
                      double lon,
                      double course,
                      double weather,
                      double alt,
                      int battery,
                      int repeater,
                      int photoNumber,
                      int satCount,
                      const QString name);


public slots:
    void readBluetooth();
    Q_INVOKABLE void bluetoothConnect(QString address);
    Q_INVOKABLE void debugMessage();
    Q_INVOKABLE void updateRetr();
    Q_INVOKABLE void setDisplayName(QString droneName, QVariant alt, QVariant numberPhoto, QVariant weather);
    Q_INVOKABLE void setTrackedDroneName(QString droneName, QVariant lat, QVariant lon);

    void elapsed_retr();
    void btConnect();
    void btDisconnect();
    void parsedDroneMessage(double lat,
                            double lon,
                            double course,
                            double weather,
                            double alt,
                            int battery,
                            int repeater,
                            const QString name,
                            int term_bat,
                            int photoNumber,
                            int satCount);

    void parsedRepeaterMessage(int charge, int status);

private:
    ParserGraph parser;
    QBluetoothDeviceDiscoveryAgent * agent = new QBluetoothDeviceDiscoveryAgent;
    QBluetoothSocket * socket;

    bool startWPread = false;
    uint8_t last_wp_id = -1;
    uint8_t wp_count = 0;

    QString reveived;

    QVariant m_lat=0;
    QVariant m_lon=0;
    QVariant m_course = 0;
    QVariant m_weather = 0;
    QVariant m_alt = 0;
    QVariant m_retCharge = 0;
    QVariant m_retStatus = 0;
    QVariant m_term_charge = 0;

    int retr_elapsed = 100000; // over then 60s
    QTime time;
    QVariant m_btConnect = false;

    QVariant droneName = false;
    QVariant trackedDroneName = false;
};

#endif // MAVCOMMUNICATOR_H
