import QtQuick 2.9
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.2
import "items"


Item {
    property bool switcherIsVisible: (adderLeft.visible == true || adderRight.visible == true ) ? false : true
    property double buttonsScale: 0
    property double buttonsOpacity: 0
    property bool enableSending: false
    property bool visibilityControls: true
    property var coordinateForDist: false
    property int distanceToCoord: 0;
    property var remotePoints: []
    property var remoteLines: []


    PositionSource {
        id: src
        updateInterval: 500
        active: enableSending
        property variant lastCoord: false
        property bool alwaysCenter: true

        onPositionChanged: {
            var coord = src.position.coordinate;

            marker.coordinate = QtPositioning.coordinate(coord.latitude, coord.longitude);
            console.debug(coord.latitude, coord.longitude)
            track.addCoordinate(QtPositioning.coordinate(coord.latitude, coord.longitude))
            var currentCoord = QtPositioning.coordinate(coord.latitude, coord.longitude);

            if(alwaysCenter == true){
                map.center = QtPositioning.coordinate(coord.latitude, coord.longitude);
            }


            if(lastCoord != false){
                marker.rotation = 180 + currentCoord.azimuthTo(lastCoord);
                lastCoord = currentCoord;
            }

            if (coordinateForDist){
                var coord2 = QtPositioning.coordinate(coord.latitude, coord.longitude);
                distanceToCoord =  coordinateForDist.distanceTo(coord2);
            }

            lastCoord = currentCoord;



            if (registration.userID != 0){
                var request = new XMLHttpRequest()
                var rqst = 'http://vlad.skittlesinc.ru/php/add.php?id='+ registration.userID +'&lat='+ coord.latitude +'&lon=' + coord.longitude + '&alt='+ 0 +'&speed='+ 0 +'&course=' + 0 + '&cloud='+ 0 +'&prefix=5';
                request.open('GET', rqst);
                console.log(rqst);
                request.send();
            }
        }
    }

    /*TextField{
        id:debug
        anchors.top: parent.top
        anchors.topMargin: 100
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width
        height: 80
        z:3
    }*/

    Rectangle{
        id: distanceForPoint
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width * 0.3
        height: parent.height * 0.05
        color: "#F08080"
        opacity: 0.8
        visible: false
        z:2
        Text{
            text: "<b>" + distanceFormat(distanceToCoord) + "</b>"
            color: "black"
            font.pointSize: 24
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
        }
        Rectangle{
            anchors.top: parent.top
            anchors.right:parent.right
            width: 50
            height: parent.height
            color: 'black'
            Text{
                text: "X"
                color: "white"
                font.pointSize: 24
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
            }

            MouseArea{
                anchors.fill: parent
                onClicked: distanceForPoint.visible = false
            }
        }

    }

    Plugin {
        id: mapPlugin
        name: "esri"
    }

    Plugin {
        id: herePlugin
        name: "here"
        PluginParameter {name: "here.app_id"; value: "iyZY1zNe3BtBhx6SXLMR" }
        PluginParameter {name: "here.token"; value: "okwuRzpClvyktmqXzsaBWw" }
    }

    Plugin {
        id: mapboxPlugin
        name: "mapbox"
        PluginParameter { name: "mapbox.access_token"; value: "pk.eyJ1IjoicHJvbWlzdHJpbyIsImEiOiJjaW1wNmIzaHQwMDJ5d2FtNGNhb28zZTRsIn0.nYE56atkirjFdB5oEkpYVA" }
        PluginParameter {name: "mapbox.map_id"; value: "promistrio.1i2blkkj" }
        PluginParameter {name: "mapbox.mapping.additional_map_ids"; value: "promistrio.cdlgxz71, promistrio.4p8nigwh" }
    }

    Map{
        id: map
        anchors.fill: parent
        plugin: mapboxPlugin
        zoomLevel: 17

        MouseArea{
            anchors.fill: parent
            onPressed: {
                src.alwaysCenter = false;
            }

            onPressAndHold: {
                adderCenter.visible = true;
                //adderCenter.setFocus();
                var coord = map.toCoordinate(Qt.point(mouse.x,mouse.y))
                adderCenter.lat = coord.latitude;
                adderCenter.lon = coord.longitude;
                adderCenter.selectAll();

                changeActive.lat = coord.latitude;
                changeActive.lon = coord.longitude;

                console.log(">>>>>>>>", coord.latitude, coord.longitude);
                switcher.visible = false
            }
        }


        MapQuickItem {
            id: marker
            width: 64
            height: 64
            anchorPoint.x: image.width/2
            anchorPoint.y: image.height/2
            z:1

            rotation: - map.bearing

            sourceItem: Image {
                id: image
                source:"qrc:/img/maps-and-flags.png"
            }
        }

        ListModel {
            id: addressModel

            ListElement {
                lat: 59.0
                lon: 37.0
                address:"52"
            }
            ListElement {
                lat: 60.0
                lon: 38.0
                address:"53"
            }
            ListElement {
                lat: 59.0
                lon: 38.0
                address:"55"
            }
        }

        /*MapQuickItem{
            coordinate {
                latitude: btMessage.lat
                longitude: btMessage.lon
            }

            rotation: btMessage.course - map.bearing
            anchorPoint.x: drone_marker.width / 2;
            anchorPoint.y: drone_marker.height;
            sourceItem: Column{
                Image {id: drone_marker; source:  "qrc:/img/drone.png"}
                Text { color:"white"; anchors.horizontalCenter: parent.horizontalCenter}
            }
        }*/

        Timer {
            interval: 3000; running: true; repeat: true
            onTriggered: {
                btMessage.updateRetr();
                droneModel.updateConnection();
            }
        }


        MapItemView {

            model: droneModel
            delegate:
                MapQuickItem{
                coordinate {
                    latitude: lat
                    longitude: lon
                }
                anchorPoint.x: marker_circle.width / 2;
                anchorPoint.y: marker_circle.height;
                sourceItem: Column{
                    id: droneMarker
                    //Image {id: mrkr; source: "qrc:/img/drone.png"; rotation: course - map.bearing}


                    Rectangle {
                        id: marker_circle
                        width: 30
                        height: width
                        color: weather_color(weather)
                        border.color: parseInt(elapsed) > 10000 ? "red" : "blue"
                        border.width: 3
                        radius: width * 0.5
                    }

                    Rectangle{
                        anchors.horizontalCenter: marker_circle.horizontalCenter;
                        color: "gray"
                        width:60
                        height:40
                        opacity: 1

                        Column{
                            Text {text: name; color:"white"; anchors.horizontalCenter: marker_circle.horizontalCenter; opacity: 1}
                        }
                    }

                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        coordinateForDist = QtPositioning.coordinate(lat, lon);

                        changeActive.activeText = address;
                        changeActive.lat = lat;
                        changeActive.lon = lon;

                    }
                }
            }
        }

        MapItemView {
            model: remotePoints
            delegate:
                MapQuickItem{
                coordinate {
                    latitude: model.modelData['lat']
                    longitude: model.modelData['lon']
                }
                anchorPoint.x: mrkr2.width / 2;
                anchorPoint.y: mrkr2.height;
                sourceItem: Column{
                    Image {id: mrkr2; source: "qrc:/img/point.png" }
                    Rectangle{
                        anchors.horizontalCenter: mrkr2.horizontalCenter;
                        color: "gray"
                        width: 40
                        height:20
                        opacity: 1
                        Text {text:model.modelData['name']; color:"white"}
                    }

                }
            }
        }

        MapItemView {
            model: remoteLines
            delegate:
                MapPolyline {
                        line.width: model.modelData['width']
                        line.color: model.modelData['color']
                        path: model.modelData['path']
                    }
        }

        MapItemView {

            model: addresses
            delegate:
                MapQuickItem{
                coordinate {
                    latitude: lat
                    longitude: lon
                }
                anchorPoint.x: mrkr.width / 2;
                anchorPoint.y: mrkr.height;
                sourceItem: Column{
                    Image {id: mrkr; source: (database.activeAddress != id)? "qrc:/img/point.png" : "qrc:/img/dot.png"}
                    Rectangle{
                        anchors.horizontalCenter: mrkr.horizontalCenter;
                        color: "gray"
                        width: 40
                        height: 20
                        opacity: 1
                        Text {text:address; color:"white"; anchors.horizontalCenter: parent.horizontalCenter; anchors.verticalCenter: parent.verticalCenter}
                    }

                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        coordinateForDist = QtPositioning.coordinate(lat, lon);

                        database.activeAddress = id;

                        changeActive.activeText = address;
                        changeActive.lat = lat;
                        changeActive.lon = lon;

                    }
                }
            }
        }

        MapItemView {

            model: photoModel
            delegate:
                MapQuickItem{
                coordinate {
                    latitude: lat
                    longitude: lon
                }
                anchorPoint.x: photo_mrkr.width / 2;
                anchorPoint.y: photo_mrkr.height / 2;
                sourceItem: Column{
                    Image {id: photo_mrkr; source: (database.activeAddress != id)? "qrc:/img/blue_star.png" : "qrc:/img/dot.png" }
                    Text {
                        text: address
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.pointSize: 16
                        color: "blue"
                    }
                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        database.activeAddress = id;
                        changeActive.activeText = address;
                        changeActive.lat = lat;
                        changeActive.lon = lon;
                        console.log(address);
                    }
                }
            }
        }

        MapQuickItem {
            id: leftmarker
            width: 64
            height: 64
            anchorPoint.x: leftimage.width/2
            anchorPoint.y: leftimage.height
            z:1

            sourceItem: Image {
                id: leftimage
                source:"qrc:/img/point.png"
            }
        }

        MapQuickItem {
            id: rightmarker
            width: 64
            height: 64
            anchorPoint.x: rightimage.width/2
            anchorPoint.y: rightimage.height
            z:1

            sourceItem: Image {
                id: rightimage
                source:"qrc:/img/point.png"
            }
        }

        MapPolyline {
            id:track
            line.width: 3
            line.color: '#7fff00'
        }
    }

    DroneList{
        id:droneList
        anchors.top: map.top
        anchors.right: map.right
        width: map.width * 0.45
        height: map.height * 0.25
    }

    /*DroneFollowList{
        id: droneFollowList
        visible: false
        anchors.top: map.top
        anchors.right: droneList.left
        anchors.left: map.left
        width: map.width - droneList.width
        height: droneList.height
    }*/


    Item{
        id: sides
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 50
        anchors.bottomMargin: 50
        width: rightRow.width
        height: leftPoint.height
        Row{
            id:rightRow
            spacing: 100
            Image{
                id: leftPoint
                anchors.margins: 40
                source: "qrc:/img/left.png"
                scale: buttonsScale
                opacity: buttonsOpacity
                visible: visibilityControls
                z:2
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        visibilityControls = false;
                        adderLeft.visible = true;
                        //adderLeft.setFocus();
                        leftmarker.coordinate = src.lastCoord.atDistanceAndAzimuth(drawer.getLeftOffset(), marker.rotation - 90);
                        adderLeft.lat = leftmarker.coordinate.latitude;
                        adderLeft.lon = leftmarker.coordinate.longitude;
                        adderLeft.selectAll();

                        changeActive.lat = leftmarker.coordinate.latitude;
                        changeActive.lon = leftmarker.coordinate.longitude;

                        switcher.visible = false
                    }

                    onPressAndHold: {
                        switcher.visible = false
                        visibilityControls = false;
                        changeActive.visible = true
                        changeActive.selectAll();
                    }
                }
            }
            Image{
                id: rightPoint
                anchors.margins: 40
                source: "qrc:/img/right.png"
                opacity: buttonsOpacity
                scale: buttonsScale
                visible: visibilityControls
                z:2
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        visibilityControls = false;
                        adderRight.visible = true;
                        rightmarker.coordinate = src.lastCoord.atDistanceAndAzimuth(drawer.getRightOffset(), marker.rotation + 90);
                        adderRight.lat = rightmarker.coordinate.latitude;
                        adderRight.lon = rightmarker.coordinate.longitude;
                        adderRight.selectAll();

                        changeActive.lat = rightmarker.coordinate.latitude;
                        changeActive.lon = rightmarker.coordinate.longitude;


                        switcher.visible = false
                    }
                    onPressAndHold: {
                        distanceForPoint.visible = true;
                    }
                }
            }
        }
    }

    Image{
        id: targetPoint
        anchors.bottom: sides.top
        anchors.right: parent.right
        anchors.margins: 50
        source: "qrc:/img/target.png"
        scale: buttonsScale
        opacity: buttonsOpacity
        visible: !src.alwaysCenter
        z:2
        Layout.row: 0
        Layout.column:3
        MouseArea{
            anchors.fill: parent
            onClicked: {
                src.alwaysCenter = !src.alwaysCenter;
                map.center = marker.coordinate;
                map.bearing = 0;
            }
            /*onPressAndHold: {
                droneFollowList.visible = !droneFollowList.visible
            }*/
        }
    }


    NumberAdder{
        id: adderRight

        anchors.bottom: parent.bottom
        anchors.left: parent.left
        width: parent.width
        height: parent.height * 0.6
        visible: false
        z:4


    }

    NumberAdder{
        id: adderLeft

        anchors.bottom: parent.bottom
        anchors.left: parent.left
        width: parent.width
        height: parent.height * 0.6
        visible: false
        z:4
    }

    NumberAdder{
        id: adderCenter

        anchors.bottom: parent.bottom
        anchors.left: parent.left
        width: parent.width
        height: parent.height * 0.6
        visible: false
        z:4
    }

    NumberAdder{
        id: changeActive

        isChangeActive: true
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        width: parent.width
        height: parent.height * 0.6
        visible: false
        z:4
    }

    function sendAddress(lat, lon, address){
        if (registration.userID != 0){
            var rqst2 = 'http://vlad.skittlesinc.ru/php/add_address.php?id='+ registration.userID +'&lat='+ lat +'&lon=' + lon +'&number=' + address;
            var request2 = new XMLHttpRequest();
            request2.open('GET', rqst2);
            console.log(rqst2);
            request2.send();

            addressModel.append({"lat": lat, "lon": lon, "address": address})
        }
    }
    function getCurrentCoord(){
        return src.lastCoord;
    }
    function hideTempMarkers(){
        leftmarker.visible = false;
        rightmarker.visible = false;
    }
    function activeTextOfNewPoint(text){
        changeActive.activeText = text;
    }

    function setMapCenter(lat, lon){
        src.alwaysCenter = false;
        map.center = QtPositioning.coordinate(lat, lon);
    }
    function weather_color(value){
        if (value < 0)
            return "blue";
        if ((value >=0) && (value < 0.4))
            return "green";
        if ((value >= 0.4) && (value < 0.6))
            return "yellow";
        if ((value >= 0.6) && (value <= 1.0))
            return "red";

    }
    function setAddressPoints(points){
        remotePoints = points
    }

    function setAddressLines(lines){
        console.log("output " + lines)
        remoteLines = lines
    }
    function distanceFormat(distance){

        if( (distance >= 1000) && (distance < 10000) )
            return (distance / 1000.0).toPrecision(3);

        else if ((distance >= 10000) && (distance < 100000))
            return (distance / 1000.0).toPrecision(3);

        else if ((distance >= 100000) && (distance < 1000000)){
            return (distance / 1000.0).toPrecision(3);
        }
        else if (distance >= 1000000){
            return (distance / 1000.0).toPrecision(4);
        }
        else
            return distance
    }
}
