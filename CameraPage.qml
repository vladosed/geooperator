import QtQuick 2.9
import QtQuick.Controls 2.3
import QtMultimedia 5.8
import Qt.labs.platform 1.0
import QtQuick.Dialogs 1.0
import "items"

Item {
    id: secondPage
    property double buttonsScale: 0
    property double buttonsOpacity: 0
    property double last_lat: 0
    property double last_lon: 0
    property string last_src: ""

    Rectangle{
        anchors.fill: parent
        color : "black"
    }

    Camera {
        id: camera

        imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash

        exposure {
            exposureCompensation: -1.0
            exposureMode: Camera.ExposurePortrait
        }

        flash.mode: Camera.FlashRedEyeReduction

        imageCapture {
            onImageCaptured: {
                photoPreview.source = preview
                photoPreview.visible = true
            }
            onImageSaved: function (requestId, path) {
                var coord = mapPage.getCurrentCoord();
                if (coord)
                    preparePhoto(coord, path);

                photoAdder.photo_src = path;
                photoAdder.lat = coord.latitude;
                photoAdder.lon = coord.longitude;

                photoAdder.visible = true;
                photoAdder.selectAll();
                switcher.visible = false;

                //database.inserIntoPhotoTable(coord.latitude, coord.longitude, path, "NAN");
            }
        }
        focus {
            focusMode: Camera.FocusAuto
            focusPointMode: Camera.FocusPointCustom
        }
    }

    VideoOutput {
        source: camera
        autoOrientation: true
        anchors.fill: parent
        focus : visible // to receive focus and capture key events when visible
    }

    Image {
        id: photoPreview
        visible: false
        fillMode: Image.PreserveAspectFit
        anchors.fill: parent
        MouseArea{
            anchors.fill:parent
            onClicked: {
                photoPreview.visible = false;
                switcher.visible = true;
            }
        }
    }

    /*PhotoAdder{
        id: photoadder
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        width: parent.width
        height: 200
    }*/

    NumberAdder{
        id: photoAdder
        isPhoto: true

        anchors.bottom: parent.bottom
        anchors.left: parent.left
        width: parent.width
        height: parent.height * 0.6
        visible: false
        z:4
    }


    Image{
        id: takephoto
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottomMargin: 60 * buttonsScale
        source: "qrc:/img/take_photo.png"
        opacity: buttonsOpacity
        scale: buttonsScale
        z:2
        visible: !photoPreview.visible
        MouseArea{
            anchors.fill: parent
            onClicked: {
                camera.imageCapture.capture();
                switcher.visible = false;
            }
        }
    }


    Image{
        id: listPhoto
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.rightMargin: 40 * buttonsScale
        anchors.bottomMargin: 60 * buttonsScale
        source: "qrc:/img/list_photo.png"
        opacity: buttonsOpacity
        scale: buttonsScale
        z:2
        visible:  !photoPreview.visible
        MouseArea{
            anchors.fill: parent
            onClicked: {
                syncPage.visible = true;
                switcher.visible = false;
            }
        }
    }

    function preparePhoto(coord, src){
        secondPage.last_lat =  coord.latitude;
        secondPage.last_lon =  coord.longitude;
        secondPage.last_src =  src;
        //database.inserIntoPhotoTable(coord.latitude, coord.longitude, path, "NAN");
    }
    function savePhoto(comment){
        var lat = secondPage.last_lat;
        var lon = secondPage.last_lon;
        var src = secondPage.last_src;
        database.insertIntoPhotoTable(lat, lon, src, comment);
    }
}
