#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "models/addressesmodel.h"
#include "models/addressesdatabase.h"
#include "models/dronemodel.h"
#include "models/database.h"
#include "models/photomodel.h"
#include "controllers/ftpgetpoints.h"

#include "controllers/uploader.h"
#include "controllers/qclipboardproxy.h"
//#include "controllers/capturer.h"

#include "network/client.h"

#include "graph_reader/communicator.h"

//#include "items/vibrator.h"

int main(int argc, char *argv[])
{

//    qmlRegisterType<AddressesDataBase, 1>("AddressesDataBase", 1, 0, "AddressesDataBase");

#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    Client client;



    //Capturer capturer;
    Uploader uploader;
    // Database class
    DataBase db;
    //SQL model
    AddressesDataBase addresses;

    DroneModel droneModel;

    FTPGetPoints ftpPoints;


    PhotoModel photo;

    Communicator communicator;


    QObject::connect(&db, &DataBase::refreshModel, &addresses,
                     &AddressesDataBase::updateModel);

    QObject::connect(&db, &DataBase::refreshModel, &photo,
                     &PhotoModel::updateModel);

    QObject::connect(&client, &Client::getAddress,
                     &db, &DataBase::getAddressTcp);

    QObject::connect(&ftpPoints, &FTPGetPoints::addPoint,
                     &db, &DataBase::insertIntoAddressTableSlot);

    QObject::connect(&communicator, &Communicator::newDroneMessage, &droneModel,
                     &DroneModel::updateDrone);

    QObject::connect(&communicator, &Communicator::headerChanged, &droneModel,
                     &DroneModel::updateHeader);
    QObject::connect(&communicator, &Communicator::trackedDroneChanged, &droneModel,
                     &DroneModel::updateTrackedDrone);


    QGuiApplication app(argc, argv);

    QClipboardProxy clipboard(QGuiApplication::clipboard());

    QQmlApplicationEngine engine;

    QQmlContext * ctx = engine.rootContext();

    //ctx->setContextProperty("addresses", &addresses);

    //ctx->setContextProperty("capturer", &capturer);
    ctx->setContextProperty("btMessage", &communicator);
    ctx->setContextProperty("database", &db);
    ctx->setContextProperty("uploader", &uploader);
    ctx->setContextProperty("addresses", &addresses);
    ctx->setContextProperty("droneModel", &droneModel);
    ctx->setContextProperty("photoModel", &photo);
    ctx->setContextProperty("client", &client);
    ctx->setContextProperty("clipboard", &clipboard);
    ctx->setContextProperty("ftpPoints", &ftpPoints);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
