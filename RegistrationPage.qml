import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.3
import Qt.labs.settings 1.0
import QtPositioning 5.6

Item {
    id: reg
    anchors.fill: parent
    property double fieldScaleWidth: 0.8
    property double fieldScaleHeight: 0.1
    property int userID: parseInt(identID.text)
    property string userName: fullName.text
    property string userPhone: phoneNumber.text

    Material.theme: Material.Gray
    Material.accent: Material.Dark

    ColumnLayout{
        spacing: parent.height * 0.07
        anchors.fill: parent

        TextField{
            id: identID
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width * fieldScaleWidth
            Layout.preferredHeight: parent.height * fieldScaleHeight
            horizontalAlignment: TextInput.AlignHCenter
            inputMethodHints: Qt.ImhDigitsOnly
            EnterKey.type: Qt.EnterKeyNext
            placeholderText: qsTr("Уникальный номер")
        }
        TextField{
            id: fullName
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width * fieldScaleWidth
            Layout.preferredHeight: parent.height * fieldScaleHeight
            horizontalAlignment: TextInput.AlignHCenter
            placeholderText: qsTr("Имя и фамилия")
            EnterKey.type: Qt.EnterKeyNext
        }
        TextField{
            id: phoneNumber
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width * fieldScaleWidth
            Layout.preferredHeight: parent.height * fieldScaleHeight
            horizontalAlignment: TextInput.AlignHCenter
            inputMethodHints: Qt.ImhDigitsOnly
            EnterKey.type: Qt.EnterKeyNext
            placeholderText: qsTr("Контактный телефон")
        }
        Button{
            id: regBtn
            text: "Регистрация"
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: parent.width * fieldScaleWidth
            Layout.preferredHeight: parent.height * fieldScaleHeight
            onClicked: {
                text = "Подтвердить"
                registration.visible = false
                mapPage.visible = true
                //switcher.visible = true

                var currentCoord = QtPositioning.coordinate(0, 0);


                var rotation = 180;

                var coordinate1 = currentCoord.atDistanceAndAzimuth(1000, rotation);

                var coordinate2 = currentCoord.atDistanceAndAzimuth(1000, rotation);
                console.log(coordinate1.latitude, coordinate1.longitude);
                mapPage.enableSending = true;
            }
        }
        Settings {
            property alias id: identID.text
            property alias name: fullName.text
            property alias phone: phoneNumber.text
            property alias regBtnText: regBtn.text
        }
    }


}
