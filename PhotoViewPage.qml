import QtQuick 2.0
import QtQuick.Controls 2.3

Item {
    property string src: ""

    Image{
        id: sync
        source: "qrc:/img/sync.png"
        width: 100
        height: 100
        anchors.right: parent.right
        anchors.top: parent.top
        z:4
        fillMode: Image.PreserveAspectFit
        MouseArea{
            anchors.fill: parent
            onClicked: {
                var coord = mapPage.getCurrentCoord();
                if (coord.isValid)
                    uploader.uploadFile(coord.latitude, coord.longitude, src);
                else
                    uploader.uploadFile(0, 0, src);
            }
        }
    }

    Rectangle{
        anchors.fill: parent
        color: "black"
        Image{
            anchors.fill:parent
            source: src
        }
    }
    MouseArea{
        anchors.fill:parent
        onClicked: {
            photoViewPage.visible = false
            back.visible=true;
        }
    }

    ProgressBar {
        height: 25
        width: parent.width
        anchors.bottom: parent.bottom
    }

    /*TextField{
        id: debugField
        anchors.centerIn: parent
        width: parent.width - 100
        text: src
        color: "steelblue"
        z:3
    }*/
}
