import QtQuick 2.9
import QtQuick.Controls 2.2
import Qt.labs.platform 1.0
import Qt.labs.folderlistmodel 2.1

Item {
    id: sync
    property string photoPath: "file:///storage/emulated/0/DCIM/" // StandardPaths.writableLocation(StandardPaths.PicturesLocation)

    PhotoViewPage{
        id: photoViewPage
        anchors.fill:parent
        z:4
        visible: false
    }

    Rectangle{
        anchors.fill: parent
        color: "white"
    }
    Rectangle{
        width: parent.width
        height: 100
        anchors.left: parent.left
        anchors.top: parent.top
        color: "white"
        z:3
    }

    Image{
        id:back
        source: "qrc:/img/left.png"
        width: 100
        height: 100
        anchors.left: parent.left
        anchors.top: parent.top
        z:4
        MouseArea{
            anchors.fill: parent
            onClicked: {
                switcher.visible = true;
                syncPage.visible = false;
            }
        }
    }

    Image{
        id: syncronize
        source: "qrc:/img/sync.png"
        width: 100
        height: 100
        anchors.right: parent.right
        anchors.top: parent.top
        z:4
        visible: !photoViewPage.visible
        MouseArea{
            anchors.fill: parent
            onClicked: {
                //switcher.visible = true;
                //syncPage.visible = false;
                photoModel.sync();
            }
        }
    }

    ListView {
        anchors.fill: parent
        anchors.topMargin: 100

        FolderListModel {
            id: folderModel
            folder: photoPath
            nameFilters: ["*.jpg"]
            showDirs: false
        }

        Component {
            id: fileDelegate
            Rectangle{
                width : parent.width
                height: 100
                Row{
                    anchors.verticalCenter: parent.verticalCenter
                    Rectangle{
                        width: 100
                        anchors.margins: 100 * 0.1
                        height: 100 * 0.8
                        //source: photoPath+"/"+fileName
                        color: "green"
                    }

                    Text {
                        text: photopath
                        font.pointSize: 11
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }
                MouseArea {
                    anchors.fill:parent
                    onClicked: {
                        photoViewPage.src = "file:///" + photopath
                        photoViewPage.visible = true
                        back.visible=false;
                    }
                }
            }
        }

        model: photoModel
        delegate: fileDelegate
        //highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
    }


    /*function setText(text){
        field.text = text;
    }

    TextField{
        id: field
        anchors.centerIn: parent
        text: photoPath
        color: "steelblue"
        z:3
    }*/

}
