#include "addressesmodel.h"
#include <QDebug>


Address::Address(double lat, double lon, const QString address)
    :  m_lat(lat), m_lon(lon), m_address(address)
{
}

double Address::lat() const
{
    return m_lat;
}

double Address::lon() const
{
    return m_lon;
}

QString Address::address() const
{
    return m_address;
}


void Address::lat(double lat)
{
    this->m_lat = lat;
}

void Address::lon(double lon)
{
    this->m_lon = lon;
}

void Address::address(QString address)
{
    this->m_address = address;
}

AddressesModel::AddressesModel(QObject *parent)
    : QAbstractListModel(parent)
{
}


void AddressesModel::addAddress(double lat, double lon, const QString address)
{
    Address * pointer = new Address(lat, lon, address);

    bool isChange = false;

    for (int i = 0; i< m_points.size(); i++)
    {

            m_points[i].lat(lat);
            m_points[i].lon(lon);
            m_points[i].address(address);
            isChange = true;
            emit dataChanged(index(0, 0), index(rowCount(), 0));
            qDebug() << rowCount();

            //qDebug() << "Изменен указатель. Трек: " << track_id << " Широта: " << lat << " " << m_points.size();

            QModelIndex start_index = createIndex(0, 0);
            QModelIndex end_index = createIndex((m_points.count() - 1), 0);
            emit dataChanged(start_index, end_index);

            break;
    }

    if(!isChange)
    {
        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        m_points << *pointer;
        endInsertRows();
        qDebug() << "Новая точка "<< " Широта: " << lat << " " << m_points.size();
    }
}

int AddressesModel::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent);
    return m_points.count();
}

QVariant AddressesModel::data(const QModelIndex & index, int role) const {

    qDebug() << index.row() << " " << index.column() << " " << role;

    if (index.row() < 0 || index.row() >= m_points.count())
        return QVariant();

    const Address &point = m_points[index.row()];
    if (role == LatRole)
        return point.lat();
    else if (role == LonRole)
        return point.lon();
    else if (role == AddressRole)
        return point.address();
    return QVariant();
}

QVariant AddressesModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
            return QVariant();

        if (orientation == Qt::Horizontal)
            return QString("Column %1").arg(section);
        else
            return QString("Row %1").arg(section);
}

Qt::ItemFlags AddressesModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

bool AddressesModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole) {

        //stringList.replace(index.row(), value.toString());
        emit dataChanged(index, index);
        return true;
    }

    return false;
}

QHash<int, QByteArray> AddressesModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[LatRole] = "lat";
    roles[LonRole] = "lon";
    roles[AddressRole] = "address";
    return roles;
}
