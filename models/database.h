#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QSql>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlDatabase>
#include <QFile>
#include <QDate>
#include <QStack>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDir>
#include <QStandardPaths>
#include "network/tcppack.h"


/* Директивы имен таблицы, полей таблицы и базы данных */
#define DATABASE_HOSTNAME   "addresses"
#define DATABASE_NAME       "addresses.db"

#define TABLE                   "addresses"         // Название таблицы
#define TABLE_ADDRESS           "address"         // Вторая колонка
#define TABLE_LAT               "lat"           // Третья колонка
#define TABLE_LON               "lon"               // Четвертая колонка
#define TABLE_TYPE              "type"               // Четвертая колонка
#define TABLE_PHOTOPATH         "photopath"               // Четвертая колонка

#define ADDRESS_TYPE            "1"
#define PHOTO_TYPE              "2"
#define UPLOAD_TYPE             "3"

// Первая колонка содержит Autoincrement ID

class DataBase : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString activeAddress READ activeAddress WRITE setActiveAddress NOTIFY activeAddressChanged)
public:
    explicit DataBase(QObject *parent = 0);
    ~DataBase();
    /* Методы для непосредственной работы с классом
     * Подключение к базе данных и вставка записей в таблицу
     * */
    void connectToDataBase();
    QString activeAddress();
    void setActiveAddress(const QString &activeAddress);

private:
    // Сам объект базы данных, с которым будет производиться работа
    QSqlDatabase    db;
    QStack<int> last_id;

private:
    /* Внутренние методы для работы с базой данных
     * */
    bool openDataBase();        // Открытие базы данных
    bool restoreDataBase();     // Восстановление базы данных
    void closeDataBase();       // Закрытие базы данных
    bool createTable();         // Создание базы таблицы в базе данных
    QString m_activeAddress;


signals:
    void refreshModel();
    void activeAddressChanged();

public slots:    
    bool insertIntoAddressTable(const QString &address, const QString &lat, const QString &lon, const QString &type, const QString &photopath);

    bool insertIntoAddressTable(const QVariantList &data);      // Добавление записей в таблицу
    bool insertIntoAddressTable(const QString &fname, const QString &sname, const QString &nik);
    bool insertIntoAddressTableSlot(const QString &adress, const QString &lat,  const QString &lon);
    bool insertIntoAddressTableTemp(const QString &address, const QString &lat,  const QString &lon);

    bool insertIntoPhotoTable(const QVariantList &data);      // Добавление записей в таблицу
    bool insertIntoPhotoTable(const QString &lat, const QString &lon, const QString &path, const QString &comment);


    void getAddressTcp(TcpPack pack);

    bool removeRecord(const int id); // Удаление записи из таблицы по её id
    bool removeActiveRecord();
    void removeTemp();
    void sync();
    bool removeLastRecord();
    void syncRequestFinished(QNetworkReply* reply);
    bool changeActiveAddress(const QString & address);
    bool changeActiveAddress(const QString & address, const QString & lat, const QString & lon);




    void uploadCSV(const QString &filename);
};

#endif // DATABASE_H
