#include "database.h"
#include <QDebug>

DataBase::DataBase(QObject *parent) : QObject(parent)
{
    /*QString path(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation));
    QDir dir; // Initialize to the desired dir if 'path' is relative
              // By default the program's working directory "." is used.

    // We create the directory if needed
    if (!dir.exists(path))
        dir.mkpath(path); // You can check the success if needed*/

    connectToDataBase();
}

DataBase::~DataBase()
{

}

/* Методы для подключения к базе данных
 * */
void DataBase::connectToDataBase()
{
    /* Перед подключением к базе данных производим проверку на её существование.
     * В зависимости от результата производим открытие базы данных или её восстановление
     * */
    if(!QFile("./" DATABASE_NAME).exists()){
        this->restoreDataBase();
        qDebug() <<"БД нет";
    } else {
        this->openDataBase();
    }
}

QString DataBase::activeAddress()
{
    return m_activeAddress;
}

void DataBase::setActiveAddress(const QString &activeAddress)
{
    if (activeAddress == m_activeAddress)
        return;

    m_activeAddress = activeAddress;
    emit activeAddressChanged();
}

/* Методы восстановления базы данных
 * */
bool DataBase::restoreDataBase()
{
    // Если база данных открылась ...
    if(this->openDataBase()){
        // Производим восстановление базы данных
        return (this->createTable()) ? true : false;
    } else {
        qDebug() << "Не удалось восстановить базу данных";
        return false;
    }
    return false;
}

/* Метод для открытия базы данных
 * */
bool DataBase::openDataBase()
{
    /* База данных открывается по заданному пути
     * и имени базы данных, если она существует
     * */
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setHostName(DATABASE_HOSTNAME);
    db.setDatabaseName("./" DATABASE_NAME);
    if(db.open()){
        return true;
    } else {
        return false;
    }
}

/* Методы закрытия базы данных
 * */
void DataBase::closeDataBase()
{
    db.close();
}

/* Метод для создания таблицы в базе данных
 * */
bool DataBase::createTable()
{
    qDebug() << "Создаем первую таблицу";
    QSqlQuery query;
    if(!query.exec( "CREATE TABLE " TABLE " ("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    TABLE_ADDRESS     " TEXT(255)    NOT NULL,"
                    TABLE_LAT     " REAL         NOT NULL,"
                    TABLE_LON       " REAL         NOT NULL,"
                    TABLE_TYPE       " INTEGER         NOT NULL,"
                    TABLE_PHOTOPATH       " TEXT(255)         NOT NULL"
                    " )"
                    )){
        qDebug() << "DataBase: error of create " << TABLE;
        qDebug() << query.lastError().text();
        return false;
    }
    else { // удалить если будет создаваться вторая таблица
        qDebug() << "DB has been created";
        return true;
    }
    /*else {
        qDebug() << "Создаем следующую таблицу";
        if(!query.exec( "CREATE TABLE Photo ("
                        "id INTEGER PRIMARY KEY AUTOINCREMENT, "
                        "path TEXT(255)    NOT NULL,"
                        "comment TEXT(255)    NOT NULL,"
                        "lat REAL         NOT NULL,"
                        "lon REAL         NOT NULL"
                        " )"
                        )){
            qDebug() << "DataBase: error of create " << TABLE;
            qDebug() << query.lastError().text();
            return false;
        } else {
            return true;
        }
    }*/

    return false;
}

void DataBase::syncRequestFinished(QNetworkReply *reply)
{

    QVariant statusCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute );
    if ( !statusCode.isValid() ){
        qDebug() << "error";
        return;
    }

    int status = statusCode.toInt();

    if ( status == 200 )
    {
        QSqlQuery query;
        /* В начале SQL запрос формируется с ключами,
         * которые потом связываются методом bindValue
         * для подстановки данных из QVariantList
         * */
        /*query.prepare("DELETE FROM addresses");
        qDebug() << "Хочу все удалить";
        if(query.exec()){
            qDebug() << "Таблица адресов очищена!";
        }
        else{
            qDebug() << query.lastError().text();
        }*/
    }
    emit refreshModel();

}

bool DataBase::changeActiveAddress(const QString &address)
{
    QSqlQuery query;
    query.prepare("UPDATE " TABLE " SET address = :address WHERE id= :ID ;");
    query.bindValue(":ID", this->m_activeAddress);
    query.bindValue(":address", address);

    // Выполняем удаление
    if(!query.exec()){
        qDebug() << "error delete row " << TABLE;
        return false;
    } else {
        emit refreshModel();
        return true;
    }
    return false;
}

bool DataBase::changeActiveAddress(const QString &address, const QString &lat, const QString &lon)
{
    QSqlQuery query;

    qDebug() << lat;
    qDebug() << lon;

    QString temp_lat (lat);
    QString temp_lon (lon);


    query.prepare("UPDATE " TABLE " SET address = :address, " TABLE_LAT " = :lat, " TABLE_LON " = :lon WHERE id= :ID;");
    query.bindValue(":ID", this->m_activeAddress);
    query.bindValue(":address", address);
    query.bindValue(":lat",  temp_lat.replace(",", "."));
    query.bindValue(":lon", temp_lon.replace(",", "."));

    // Выполняем удаление
    if(!query.exec()){
        qDebug() << "error delete row " << TABLE;
        return false;
    } else {
        emit refreshModel();
        return true;
    }
    return false;
}

void DataBase::uploadCSV(const QString &filename)
{
    QString name(filename);

    qDebug()<< "файл "<< name.replace("file:///","");
    QFile file("/Download/address.txt");
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << file.errorString();
    }

    while (!file.atEnd()) {
        QByteArray line = file.readLine();
        line = line.replace("\r","");
        line = line.replace("\n","");
        line = line.replace(", ",",");
        line = line.replace(" ,",",");
        QList<QByteArray> split = line.split(',');
        qDebug() << split;

        insertIntoAddressTable(split.at(0), split.at(1), split.at(2), QString(UPLOAD_TYPE), QString(""));
        qDebug() << "Есть контакт";
    }

}

bool DataBase::insertIntoAddressTable(const QVariantList &data)
{
    QSqlQuery query;

    query.prepare("INSERT INTO " TABLE " ( " TABLE_ADDRESS ", "
                  TABLE_LAT ", "
                  TABLE_LON ", "
                  TABLE_TYPE ", "
                  TABLE_PHOTOPATH " ) "
                                  "VALUES (:address, :lat, :lon, :type, :photopath)");
    query.bindValue(":address",       data[0].toString());
    query.bindValue(":lat",           data[1].toString());
    query.bindValue(":lon",           data[2].toString());
    query.bindValue(":type",          ADDRESS_TYPE);
    query.bindValue(":photopath",     "");

    if(!query.exec()){
        qDebug() << "error insert into " << TABLE;
        qDebug() << data[0].toString() << " " << data[1].toString() << " " << data[2].toString();
        qDebug() << query.lastError().text();
        qDebug() << query.lastQuery();
        return false;
    } else {
        emit refreshModel();
        this->last_id.push(query.lastInsertId().toInt());
        qDebug() << "last id: " << query.lastInsertId();
        setActiveAddress(query.lastInsertId().toString());
        return true;
    }
    return false;
}

bool DataBase::insertIntoAddressTable(const QString &address, const QString &lat, const QString &lon, const QString &type, const QString &photopath)
{

    QSqlQuery query;

    query.prepare("INSERT INTO " TABLE " ( " TABLE_ADDRESS ", "
                  TABLE_LAT ", "
                  TABLE_LON ", "
                  TABLE_TYPE ", "
                  TABLE_PHOTOPATH " ) "
                                  "VALUES (:address, :lat, :lon, :type, :photopath)");
    query.bindValue(":address",       address);
    query.bindValue(":lat",           lat);
    query.bindValue(":lon",           lon);
    query.bindValue(":type",          type);
    query.bindValue(":photopath",     photopath);


    if(!query.exec()){
        qDebug() << "error insert into " << TABLE;
        qDebug() << address << " " << lat << " " << lon;
        qDebug() << query.lastError().text();
        qDebug() << query.lastQuery();
        return false;
    } else {
        emit refreshModel();
        this->last_id.push(query.lastInsertId().toInt());
        qDebug() << "last id: " << query.lastInsertId();
        setActiveAddress(query.lastInsertId().toString());
        return true;
    }
    return false;
}


/* Второй метод для вставки записи в базу данных
 * */
bool DataBase::insertIntoAddressTable(const QString &fname, const QString &sname, const QString &nik)
{
    QVariantList data;
    data.append(fname);
    data.append(sname);
    data.append(nik);


    if(insertIntoAddressTable(data))
    {
        return true;
    }

    else
        return false;
}

bool DataBase::insertIntoAddressTableSlot(const QString &adress, const QString &lat,  const QString &lon)
{
    QVariantList data;
    data.append(adress);
    data.append(lat);
    data.append(lon);


    if(insertIntoAddressTable(data))
    {
        return true;
    }
    return false;

}

bool DataBase::insertIntoAddressTableTemp(const QString &address, const QString &lat, const QString &lon)
{
    QString type(UPLOAD_TYPE);
    QString photopath = QString("");
    insertIntoAddressTable(address, lat, lon, type, photopath);
}

bool DataBase::insertIntoPhotoTable(const QVariantList &data)
{
    QSqlQuery query;

    query.prepare("INSERT INTO addresses ( lat, lon, photopath, address, type) "
                  "VALUES (:lat, :lon, :path, :comment, :type)");
    query.bindValue(":lat",       data[0].toString());
    query.bindValue(":lon",       data[1].toString());
    query.bindValue(":path",      data[2].toString());
    query.bindValue(":comment",   data[3].toString());
    query.bindValue(":type",      PHOTO_TYPE);


    if(!query.exec()){
        qDebug() << "error insert into " << "Photo";
        qDebug() << data[0].toString() << " " << data[1].toString() << " " << data[2].toString() << " " << data[3].toString();
        qDebug() << query.lastError().text();
        return false;
    } else {
        setActiveAddress(query.lastInsertId().toString());
        emit refreshModel();
        /*this->last_id.push(query.lastInsertId().toInt());
        qDebug() << "last id: " << query.lastInsertId();*/
        return true;
    }
    return false;
}

bool DataBase::insertIntoPhotoTable(const QString &lat, const QString &lon, const QString &path, const QString &comment)
{

    QVariantList data;
    data.append(lat);
    data.append(lon);
    data.append(path);
    data.append(comment);

    if(insertIntoPhotoTable(data))
    {
        return true;
    }

    else
        return false;

}

void DataBase::getAddressTcp(TcpPack pack)
{
    QString lat = QString::number(pack.lat());
    QString lon = QString::number(pack.lon());
    QString address = pack.str();
    QString type(UPLOAD_TYPE);
    QString photopath = QString("");
    insertIntoAddressTable(address, lat, lon, type, photopath);
}

/* Метод для удаления записи из таблицы
 * */
bool DataBase::removeRecord(const int id)
{
    // Удаление строки из базы данных будет производитсья с помощью SQL-запроса
    QSqlQuery query;

    // Удаление производим по id записи, который передается в качестве аргумента функции
    query.prepare("DELETE FROM " TABLE " WHERE id= :ID ;");
    query.bindValue(":ID", id);

    // Выполняем удаление
    if(!query.exec()){
        qDebug() << "error delete row " << TABLE;
        return false;
    } else {
        emit refreshModel();
        return true;
    }
    return false;
}

bool DataBase::removeActiveRecord()
{
    qDebug() << m_activeAddress.toInt();
    if(!m_activeAddress.isEmpty()){

        return removeRecord(m_activeAddress.toInt());
    }
    else{
        return false;
    }
}

void DataBase::removeTemp()
{
    QSqlQuery query;
    /* В начале SQL запрос формируется с ключами,
     * которые потом связываются методом bindValue
     * для подстановки данных из QVariantList
     * */
    query.prepare("DELETE FROM addresses WHERE type=" UPLOAD_TYPE);
    qDebug() << "Хочу все удалить";
    if(query.exec()){
        qDebug() << "Временные адреса!";
    }
    else{
        qDebug() << query.lastError().text();
    }
    emit refreshModel();

}

void DataBase::sync()
{
    QSqlQuery query("SELECT *  FROM addresses");

    QJsonArray mainArray;

    while (query.next()) {
        QString id = query.value(0).toString();
        QString address = query.value(1).toString();
        QString lat = query.value(2).toString();
        QString lon = query.value(3).toString();
        qDebug() << id << " " << address << " " << lat << " " << lon;

        QJsonArray rowArray;
        rowArray.push_back(address);
        rowArray.push_back(lat);
        rowArray.push_back(lon);

        mainArray.push_back(rowArray);
    }


    QJsonDocument doc(mainArray);



    QUrl url("http://vlad.skittlesinc.ru/php/addresses/sync.php");
    QNetworkRequest request(url);

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");


    QNetworkAccessManager *nam = new QNetworkAccessManager(this);

    nam->post(request, doc.toJson());

    connect(nam, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(syncRequestFinished(QNetworkReply*)));
}



bool DataBase::removeLastRecord()
{

    if(!last_id.isEmpty()){
        return removeRecord(this->last_id.pop());
    }
    else{
        return false;
    }
}
