#include "photomodel.h"
#include "database.h"

PhotoModel::PhotoModel(QObject *parent) :
    QSqlQueryModel(parent)
{
    this->updateModel();
}

// Метод для получения данных из модели
QVariant PhotoModel::data(const QModelIndex & index, int role) const {

    // Определяем номер колонки, адрес так сказать, по номеру роли
    int columnId = role - Qt::UserRole - 1;
    // Создаём индекс с помощью новоиспечённого ID колонки
    QModelIndex modelIndex = this->index(index.row(), columnId);

    /* И с помощью уже метода data() базового класса
     * вытаскиваем данные для таблицы из модели
     * */
    return QSqlQueryModel::data(modelIndex, Qt::DisplayRole);
}

void PhotoModel::sync()
{
    uploader = new Uploader();

    QSqlQuery query;
    query.exec("SELECT id, lat, lon, path, comment FROM Photo");
    while (query.next()) {
        QString id = query.value(0).toString();
        double lat = query.value(1).toDouble();
        double lon = query.value(2).toDouble();
        QString path = query.value(3).toString();
        //QString comment = query.value(4).toString();

        uploader->uploadFile(lat, lon, path);

        QEventLoop loop;
        QObject::connect(uploader, &Uploader::uploadFinished, &loop, &QEventLoop::quit);
        loop.exec();

        /*QSqlQuery query2;
        query2.exec(QString("DELETE FROM Photo WHERE id = ") + id);*/

        this->updateModel();
    }
    this->updateModel();
}

void PhotoModel::syncLocal()
{
    QString filename("/storage/emulated/0/NewFile");
    QFile file (filename);
    file.open(QIODevice::WriteOnly); // Or QIODevice::ReadWrite

    QTextStream out(&file);

    QSqlQuery query;
    query.exec("SELECT id, lat, lon, photopath, address FROM addresses WHERE type = " PHOTO_TYPE);

    while (query.next()) {
        QString id = query.value(0).toString();
        double lat = query.value(1).toDouble();
        double lon = query.value(2).toDouble();
        QString path = query.value(3).toString();
        QString comment = query.value(4).toString();

        out << lat << "\t";
        out << lon << "\t";
        out << path << "\t";
        out << comment << "\t";
        out << endl;

        this->updateModel();
    }

    file.close();

}


QHash<int, QByteArray> PhotoModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[LatRole] = "lat";
    roles[LonRole] = "lon";
    roles[PathRole] = "photopath";
    roles[CommentRole] = "address";
    return roles;
}

// Метод обновления таблицы в модели представления данных
void PhotoModel::updateModel()
{
    // Обновление производится SQL-запросом к базе данных
    this->setQuery("SELECT id, lat, lon, photopath, address FROM addresses WHERE type = " PHOTO_TYPE);
}

// Получение id из строки в модели представления данных
int PhotoModel::getId(int row)
{
    return this->data(this->index(row, 0), IdRole).toInt();
}
