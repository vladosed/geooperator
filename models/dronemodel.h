#ifndef DRONEMODEL_H
#define DRONEMODEL_H



#include <QObject>
#include <QAbstractListModel>
#include <QList>
#include <QGeoCoordinate>
#include <QDebug>
#include <QTime>
#include <QTimer>

class Drone
{
public:
    Drone(double lat, double lon, double course, double weather, double alt, int timestamp, int battery, int repeater, int photoNumber, int satCount, const QString name);

    double lat() const;
    double lon() const;
    double course() const;
    double weather() const;
    double alt() const;
    int elapsed() const;
    QString name() const;
    QString battery() const;
    int repeater() const;
    int photoNumber() const;
    int satCount() const;
    bool selected() const;

    void lat(double lat);
    void lon(double lon);
    void course(double course);
    void weather(double weather);
    void alt(double alt);
    void elapsed();
    void name(QString name);
    void battery(int bt);
    void repeater(int repeater);
    void photoNumber(int photo_number);
    void satCount(int sat_count);
    void selected(bool value);

private:
    double m_lat;
    double m_lon;
    double m_course;
    double m_weather;
    double m_alt;
    int m_elapsed;
    int m_battery;
    int m_repeater;
    int m_photoNumber;
    int m_satCount;
    bool m_selected = false;

    QString m_name;
    QTime time;
};


class DroneModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(QVariant alt READ getAlt NOTIFY altChanged)
    Q_PROPERTY(QVariant weather READ getWeather NOTIFY weatherChanged)
    Q_PROPERTY(QVariant photoNumber READ getPhotoNumber NOTIFY photoNumberChanged)
    Q_PROPERTY(QVariant lat READ getLat NOTIFY latChanged)
    Q_PROPERTY(QVariant lon READ getLon NOTIFY lonChanged)
public:
    enum PointRoles {
        LatRole = Qt::UserRole + 1,
        LonRole,
        CourseRole,
        WeatherRole,
        AltRole,
        NameRole,
        BatteryRole,
        RepeaterRole,
        PhotoRole,
        SatRole,
        ElapsedRole,
    };

    DroneModel(QObject *parent = 0);

    QVariant getAlt();
    QVariant getWeather();
    QVariant getPhotoNumber();
    QVariant getLat();
    QVariant getLon();


    int rowCount(const QModelIndex & parent = QModelIndex()) const;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;

    Qt::ItemFlags flags(const QModelIndex &index) const;

    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole);
public slots:
    Q_INVOKABLE void updateDrone(double lat, double lon, double course, double weather, double alt, int battery, int repeater, int photoNumber,int satCount, const QString name);
    Q_INVOKABLE void updateConnection();
    Q_INVOKABLE void updateHeader(QVariant alt, QVariant numberPhoto, QVariant weather);
    Q_INVOKABLE void updateTrackedDrone(QVariant lat, QVariant lon);


signals:
    void altChanged();
    void weatherChanged();
    void photoNumberChanged();
    void latChanged();
    void lonChanged();

protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<Drone> m_drones;
    QTime timer;
    QTimer * mainTimer;

    QVariant m_current_alt = 0;
    QVariant m_weather = 0;
    QVariant m_photo_number = 0;
    QVariant m_lat = 0;
    QVariant m_lon = 0;

};

#endif // DRONEMODEL_H
