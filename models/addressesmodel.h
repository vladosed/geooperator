#ifndef ADDRESSESMODEL_H
#define ADDRESSESMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QList>
#include <QGeoCoordinate>
#include <QDebug>

class Address
{
public:
    Address(double lat, double lon, const QString address);

    double lat() const;
    double lon() const;
    QString address() const;

    void lat(double lat);
    void lon(double lon);
    void address(QString address);

private:
    double m_lat;
    double m_lon;
    QString m_address;
};


class AddressesModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum PointRoles {
        LatRole = Qt::UserRole + 1,
        LonRole,
        AddressRole
    };

    AddressesModel(QObject *parent = 0);


    Q_INVOKABLE void addAddress(double lat, double lon, const QString address);

    int rowCount(const QModelIndex & parent = QModelIndex()) const;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;

    Qt::ItemFlags flags(const QModelIndex &index) const;

    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole);


protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<Address> m_points;
};


#endif // ADDRESSESMODEL_H
