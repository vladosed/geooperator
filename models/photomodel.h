#ifndef PHOTOMODEL_H
#define PHOTOMODEL_H

#include <QObject>
#include <QSqlQueryModel>
#include "controllers/uploader.h"

class PhotoModel: public QSqlQueryModel
{
    Q_OBJECT
public:
    enum Roles {
        IdRole = Qt::UserRole + 1,
        LatRole,
        LonRole,
        PathRole,
        CommentRole
    };

    explicit PhotoModel(QObject *parent = 0);

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    Q_INVOKABLE void sync();
    Q_INVOKABLE void syncLocal();

protected:
    QHash<int, QByteArray> roleNames() const;

signals:

public slots:
    void updateModel();
    int getId(int row);

private:
    Uploader * uploader;
};

#endif // PHOTOMODEL_H
