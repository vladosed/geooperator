#ifndef ADDRESSESDATABASE_H
#define ADDRESSESDATABASE_H


#include <QObject>
#include <QSqlQueryModel>

class AddressesDataBase : public QSqlQueryModel
{
    Q_OBJECT
public:
    enum Roles {
        IdRole = Qt::UserRole + 1,      // id
        AddressRole,                    // имя
        LatRole,                        // фамилия
        LonRole                         // ник
    };

    explicit AddressesDataBase(QObject *parent = 0);

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

protected:

    QHash<int, QByteArray> roleNames() const;

signals:

public slots:
    void updateModel();
    int getId(int row);
};

#endif // ADDRESSESDATABASE_H
