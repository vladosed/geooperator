#include "dronemodel.h"
#include <QDebug>


Drone::Drone(double lat, double lon, double course, double weather,  double alt, int timestamp, int battery, int repeater, int photoNumber, int satCount, const QString name)
    :  m_lat(lat), m_lon(lon), m_course(course), m_weather(weather), m_alt(alt), m_elapsed(timestamp), m_battery(battery), m_repeater(repeater), m_photoNumber(photoNumber), m_satCount(satCount),  m_name(name)
{
    time.start();
    this->elapsed();

    this->m_selected = false;
}

double Drone::lat() const
{
    return m_lat;
}

double Drone::lon() const
{
    return m_lon;
}

double Drone::course() const
{
    return m_course;
}

double Drone::weather() const
{
    return m_weather;
}

double Drone::alt() const
{
    return m_alt;
}

int Drone::elapsed() const
{
    return time.elapsed() - m_elapsed;
}

QString Drone::name() const
{
    return m_name;
}

QString Drone::battery() const
{
    double val = (m_battery * 5.0 ) / 255.0;
    return QString::number(val, 'f', 1);
}

int Drone::repeater() const
{
    return m_repeater;
}

int Drone::photoNumber() const
{
    return m_photoNumber;
}

int Drone::satCount() const
{
    return m_satCount;
}

bool Drone::selected() const
{
    return m_selected;
}

void Drone::lat(double lat)
{
    this->m_lat = lat;
}

void Drone::lon(double lon)
{
    this->m_lon = lon;
}

void Drone::course(double course)
{
    this->m_course = course;
}

void Drone::weather(double weather)
{
    this->m_weather = weather;
}

void Drone::alt(double alt)
{
    this->m_alt = alt;
}

void Drone::elapsed()
{
    this->m_elapsed = time.elapsed();
}

void Drone::name(QString name)
{
    this->m_name = name;
}

void Drone::battery(int bt)
{
    this->m_battery = bt;
}

void Drone::repeater(int repeater)
{
    this->m_repeater = repeater;
}

void Drone::photoNumber(int photo_number)
{
    this->m_photoNumber = photo_number;
}

void Drone::satCount(int sat_count)
{
    this->m_satCount = sat_count;
}

void Drone::selected(bool value)
{
    this->m_selected = value;
}

DroneModel::DroneModel(QObject *parent)
    : QAbstractListModel(parent)
{
    timer.start();
    qDebug() << "Timer Start";
}

QVariant DroneModel::getAlt()
{
    return m_current_alt;
}

QVariant DroneModel::getWeather()
{
    return m_weather;
}

QVariant DroneModel::getPhotoNumber()
{
    return m_photo_number;
}

QVariant DroneModel::getLat()
{
    return m_lat;
}

QVariant DroneModel::getLon()
{
    return m_lon;
}

void DroneModel::updateHeader(QVariant alt, QVariant numberPhoto, QVariant weather)
{
    qDebug() << alt << " " << numberPhoto << " " << weather;

    m_current_alt = alt;
    m_weather = weather;
    m_photo_number = numberPhoto;

    emit altChanged();
    emit weatherChanged();
    emit photoNumberChanged();
}

void DroneModel::updateTrackedDrone(QVariant lat, QVariant lon)
{
    m_lat = lat;
    m_lon = lon;

    emit latChanged();
    emit lonChanged();
}

void DroneModel::updateDrone(double lat, double lon, double course, double weather, double alt, int battery, int repeater, int photoNumber, int satCount, const QString name)
{
    bool isChange = false;

    for (int i = 0; i< m_drones.size(); i++)
    {
        if(m_drones[i].name() == name){
            m_drones[i].lat(lat);
            m_drones[i].lon(lon);
            m_drones[i].course(course);
            m_drones[i].weather(weather);
            m_drones[i].elapsed();
            m_drones[i].alt(alt);
            m_drones[i].battery(battery);
            m_drones[i].repeater(repeater);
            m_drones[i].photoNumber(photoNumber);
            m_drones[i].satCount(satCount);
            isChange = true;
            emit dataChanged(index(0, 0), index(rowCount(), 0));

            //qDebug() << "Изменен указатель. Трек: " << track_id << " Широта: " << lat << " " << m_drones.size();

            QModelIndex start_index = createIndex(0, 0);
            QModelIndex end_index = createIndex((m_drones.count() - 1), 0);
            emit dataChanged(start_index, end_index);

            break;
        }
    }

    if(!isChange)
    {
        auto * pointer = new Drone(lat, lon, course, weather, alt, timer.elapsed(), battery, repeater, photoNumber, satCount, name);
        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        m_drones << *pointer;
        endInsertRows();
        qDebug() << "Новая точка "<< " Широта: " << lat << " " << m_drones.size();
    }
}

void DroneModel::updateConnection()
{
    for (int i = 0; i< m_drones.size(); i++)
    {
        emit dataChanged(index(0, 0), index(rowCount(), 0));
        qDebug() << rowCount();

        QModelIndex start_index = createIndex(0, 0);
        QModelIndex end_index = createIndex((m_drones.count() - 1), 0);
        emit dataChanged(start_index, end_index);

        break;
    }
}



int DroneModel::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent);
    return m_drones.count();
}

QVariant DroneModel::data(const QModelIndex & index, int role) const {

    //qDebug() << index.row() << " " << index.column() << " " << role;

    if (index.row() < 0 || index.row() >= m_drones.count())
        return QVariant();

    const Drone &point = m_drones[index.row()];
    if (role == LatRole)
        return point.lat();
    else if (role == LonRole)
        return point.lon();
    else if (role == CourseRole)
        return point.course();
    else if (role == WeatherRole)
        return point.weather();
    else if (role == AltRole)
        return point.alt();
    else if (role == ElapsedRole)
        return point.elapsed();
    else if (role == NameRole)
        return point.name();
    else if (role == BatteryRole)
        return point.battery();
    else if (role == RepeaterRole)
            return point.repeater();
    else if (role == SatRole)
            return point.satCount();
    else if (role == PhotoRole)
            return point.photoNumber();
    return QVariant();
}

QVariant DroneModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
        return QString("Column %1").arg(section);
    else
        return QString("Row %1").arg(section);
}

Qt::ItemFlags DroneModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

bool DroneModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole) {

        //stringList.replace(index.row(), value.toString());
        emit dataChanged(index, index);
        return true;
    }

    return false;
}

QHash<int, QByteArray> DroneModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[LatRole] = "lat";
    roles[LonRole] = "lon";
    roles[CourseRole] = "course";
    roles[WeatherRole] = "weather";
    roles[AltRole] = "alt";
    roles[ElapsedRole] = "elapsed";
    roles[NameRole] = "name";
    roles[BatteryRole] = "battery";
    roles[RepeaterRole] = "repeater";
    roles[SatRole] = "sat";
    roles[PhotoRole] = "photo";

    return roles;
}
