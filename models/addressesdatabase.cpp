#include "addressesdatabase.h"
#include "database.h"

AddressesDataBase::AddressesDataBase(QObject *parent) :
    QSqlQueryModel(parent)
{
    this->updateModel();
}

// Метод для получения данных из модели
QVariant AddressesDataBase::data(const QModelIndex & index, int role) const {

    // Определяем номер колонки, адрес так сказать, по номеру роли
    int columnId = role - Qt::UserRole - 1;
    // Создаём индекс с помощью новоиспечённого ID колонки
    QModelIndex modelIndex = this->index(index.row(), columnId);

    /* И с помощью уже метода data() базового класса
     * вытаскиваем данные для таблицы из модели
     * */
    return QSqlQueryModel::data(modelIndex, Qt::DisplayRole);
}

// Метод для получения имен ролей через хешированную таблицу.
QHash<int, QByteArray> AddressesDataBase::roleNames() const {
    /* То есть сохраняем в хеш-таблицу названия ролей
     * по их номеру
     * */
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[AddressRole] = "address";
    roles[LatRole] = "lat";
    roles[LonRole] = "lon";
    return roles;
}

// Метод обновления таблицы в модели представления данных
void AddressesDataBase::updateModel()
{
    // Обновление производится SQL-запросом к базе данных
    this->setQuery("SELECT id, " TABLE_ADDRESS ", " TABLE_LAT",  " TABLE_LON " FROM " TABLE " WHERE type IN(" ADDRESS_TYPE "," UPLOAD_TYPE")");
}

// Получение id из строки в модели представления данных
int AddressesDataBase::getId(int row)
{
    return this->data(this->index(row, 0), IdRole).toInt();
}
