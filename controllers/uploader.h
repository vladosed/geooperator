#ifndef UPLOADER_H
#define UPLOADER_H


#include <QObject>

#include <QtCore>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QHttpPart>
#include <QUrl>
#include <QBuffer>
#include <QPixmap>

class Uploader : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariant progress READ progress NOTIFY progressChanged)


private slots:
    void replyFinished();
    void uploadProgress(qint64 one, qint64 two);
signals:
    void progressChanged();
public:
    Uploader()
    {
        this->m_progress = 0;
    }
    Q_INVOKABLE void uploadFile(double lat, double lon, QString path);
    QVariant progress();
private:
    QNetworkAccessManager *manager;
    double m_progress;
signals:
    void uploadFinished();
};

#endif // UPLOADER_H
