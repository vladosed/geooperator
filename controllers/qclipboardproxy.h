#ifndef QCLIPBOARDPROXY_H
#define QCLIPBOARDPROXY_H


#include <QObject>
#include <QClipboard>

class QClipboard;

class QClipboardProxy : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString text READ text NOTIFY textChanged)
public:
    explicit QClipboardProxy(QClipboard*);

    Q_INVOKABLE void set(QString str);

    QString text();

signals:
    void textChanged();

private:
    QClipboard* clipboard;
};


#endif // QCLIPBOARDPROXY_H
