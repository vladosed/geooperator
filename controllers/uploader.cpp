#include "uploader.h"


void Uploader::replyFinished()
{
    qDebug() << "OK";
    emit uploadFinished();
}

void Uploader::uploadProgress(qint64 one, qint64 two)
{
    qDebug() << one << "/" << two;
    this->m_progress = (one * 1.0) / (two);
    emit progressChanged();
}


void Uploader::uploadFile(double lat, double lon, QString path)
{
    //path.replace("file:///", ""); // windows
    //path.replace("file:///", "/"); // linux/android
    manager = new QNetworkAccessManager(this);
    QUrl testUrl("http://vlad.skittlesinc.ru/php/upload.php");
    QNetworkRequest request(testUrl);



    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QString preview_path  = path;

    QHttpPart previewPathPart;
    previewPathPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"preview_path\""));
    previewPathPart.setBody(preview_path.toLatin1());

    QHttpPart latPart;
    latPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"lat\""));
    latPart.setBody(QString::number(lat).toLatin1());

    QHttpPart lonPart;
    lonPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"lon\""));
    lonPart.setBody(QString::number(lon).toLatin1());

    QHttpPart previewFilePart;
    previewFilePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant( "image/jpeg"));
    previewFilePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"preview_file\""));


    if(!QFile(path).exists()){
        qDebug() << "file not found";
    }

    QFile *file = new QFile(preview_path);

    file->open(QIODevice::ReadOnly);
    previewFilePart.setBodyDevice(file);
    file->setParent(multiPart); // we cannot delete the file now, so delete it with the multiPart

    multiPart->append(previewPathPart);
    multiPart->append(previewFilePart);
    multiPart->append(latPart);
    multiPart->append(lonPart);

    QNetworkReply * reply = manager->post(request, multiPart);
    multiPart->setParent(reply); // delete the multiPart with the reply

    qDebug() << path << " " << lat <<  " " << lon;

    connect(reply, SIGNAL(finished()),
              this, SLOT (replyFinished()));
    connect(reply, SIGNAL(uploadProgress(qint64, qint64)),
            this, SLOT  (uploadProgress(qint64, qint64)));
}

QVariant Uploader::progress()
{
    return QVariant();
}

