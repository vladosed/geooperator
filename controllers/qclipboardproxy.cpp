#include "qclipboardproxy.h"

#include <QClipboard>

QClipboardProxy::QClipboardProxy(QClipboard* c) : clipboard(c)
{
    connect(c, &QClipboard::dataChanged, this, &QClipboardProxy::textChanged);
}

void QClipboardProxy::set(QString str)
{
    clipboard->setText(str);
}

QString QClipboardProxy::text()
{
    return clipboard->text();
}
