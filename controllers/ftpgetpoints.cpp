#include "ftpgetpoints.h"

FTPGetPoints::FTPGetPoints()
{
    m_manager = new QNetworkAccessManager;
}

void FTPGetPoints::getPoints()
{


}

void FTPGetPoints::downloadPoints()
{
    m_file = new QFile("inputFile.txt");
    // Пробуем открыть файл
    if (!m_file->open(QIODevice::WriteOnly))
    {
        delete m_file;
        m_file = nullptr;
        return;
    }



    // Создаём запрос
    QUrl url(QString("ftp://p182093.ftp.ihc.ru/temp/VladB/111/4.TXT"));
    url.setUserName("p182093_aeromon");
    url.setPassword("SqcY6HCvsZ");
    url.setPort(21);

    QNetworkRequest request(url);

    // Обязательно разрешаем переходить по редиректам
    request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    // Запускаем скачивание

    m_currentReply = m_manager->get(request);
    qDebug() << "after get";

    // После чего сразу подключаемся к сигналам о готовности данных к чтению и обновлению прогресса скачивания
    connect(m_currentReply, &QNetworkReply::readyRead, this, &FTPGetPoints::onReadyRead);
}

void FTPGetPoints::onReadyRead()
{
    QByteArray raw_message (m_currentReply->readAll());
    if (m_file)
    {
        QStringList list;
        QString str (raw_message);
        list = str.split("\r\n");

        for(int i = 0; i < list.size(); i++){
            QString line = list.at(i);
            if (line.contains("#9", Qt::CaseInsensitive)){
                QString semantic = line.remove("#9").remove(' ').remove("\"");
                QString count = list.at(i+1);
                QStringList coord = list.at(i+2).split(' ');
                qDebug() << semantic << coord;
                emit addPoint(semantic, coord.at(0), coord.at(1));
            }

        }
        //m_file->write(raw_message);
        //qDebug() << "Записал в файл";
    }
}
