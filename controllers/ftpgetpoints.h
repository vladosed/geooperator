#ifndef FTPGETPOINTS_H
#define FTPGETPOINTS_H
#include <QObject>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QFile>


class FTPGetPoints : public QObject
{
    Q_OBJECT
public:
    FTPGetPoints();
    Q_INVOKABLE void getPoints();
public slots:
    Q_INVOKABLE void downloadPoints();
private slots:
    void onReadyRead();
signals:
    bool addPoint(const QString &adress, const QString &lat, const QString &lon);

private:
    QNetworkAccessManager *m_manager;
    QString m_fileName;
    QFile *m_file;
    QNetworkReply* m_currentReply {nullptr};


};

#endif // FTPGETPOINTS_H
